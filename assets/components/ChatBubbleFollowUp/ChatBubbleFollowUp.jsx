import React from "react";
import "./ChatBubbleFollowUp.scss";
import { formatDate, renderContent, stringAvatar } from "../../services/Utils";
import { Avatar, Tooltip } from "@mui/material";
import { Check, DeleteOutline, EditOutlined } from "@mui/icons-material";
import { useSelector } from "react-redux";
import { url } from "../../urlApi";

function ChatBubbleFollowUp({
  item,
  decodJwt,
  setError,
  setDeleteClicked,
  deleteClicked,
  isOpen,
  setIsOpen,
  setIsEdit,
  setEditorDesc,
  setIdFollowUp,
}) {
  const token = useSelector((state) => state.auth.token);
  const handleDelete = async (e, id) => {
    e.preventDefault();
    try {
      const response = await fetch(url.Put_deleted_followup + "/" + id, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + token,
        },
        body: JSON.stringify({
          deleted: 1,
        }),
      });
      if (response.ok) {
        console.log("deleted successfully:" + response);
      } else {
        console.log("deleted error:" + response);
      }
      setDeleteClicked(!deleteClicked);
    } catch (error) {
      console.error("Error catched:" + error);
    }
  };
  const handleEdit = (item) => {
    setIsOpen(false);
    setIsEdit(true);
    setEditorDesc(item.content);
    setIdFollowUp(item.id);
  };
  function bgColor(solution) {
    switch (solution) {
      case 0:
        return "#8e5c96";
      case 1:
        return "#78bb7b";
      case 2:
        return "lightred";

      default:
        break;
    }
  }
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
      }}
    >
      <p
        style={{
          margin: "0 auto 5px auto",
          fontSize: "10px",
          opacity: "0.5",
        }}
      >
        {formatDate(item.CreatedAt)}
      </p>
      <div
        style={{
          display: "flex",
          justifyContent: "end",
          marginRight: "10px",
        }}
      >
        <div>
          <Tooltip
            title={`${item.users.firstName + " " + item.users.lastName}`}
          >
            <Avatar
              style={{
                width: "30px",
                height: "30px",
                fontSize: "12px",
                color: "black",
              }}
              {...stringAvatar(
                item.users.firstName + " " + item.users.lastName
              )}
            />
          </Tooltip>
          {item.solution == 1 && (
            <Check sx={{ color: "#1ea325" }} fontSize="large" />
          )}
        </div>
        <div
          className="chat-bubble followUp"
          style={{ backgroundColor: bgColor(item.solution) }}
        >
          <div dangerouslySetInnerHTML={renderContent(item.content)} />
        </div>
        {item.users.id == decodJwt.id && item.solution == 0 && (
          <div style={{ margin: "auto 0", display: "flex" }}>
            <DeleteOutline
              onClick={(e) => handleDelete(e, item.id)}
              fontSize="small"
              className="FollowUpIcon"
            />
            <EditOutlined
              onClick={() => handleEdit(item)}
              fontSize="small"
              className="FollowUpIcon"
            />
          </div>
        )}
      </div>
    </div>
  );
}

export default ChatBubbleFollowUp;
