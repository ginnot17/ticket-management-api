import React from "react";
import "./Menu.scss";
import { ContactPage, Logout, PostAdd, Visibility } from "@mui/icons-material";
import { useDispatch, useSelector } from "react-redux";
import { clearToken } from "../../slice/AuthSlice";
import { useNavigate } from "react-router-dom";
import { useParams } from "react-router-dom";
function Menu() {
  let { menu } = useParams();
  const active = useSelector((state) => state.ActiveUmberger.value);
  const dispatch = useDispatch();
  const handleLogout = () => {
    const confirmation = window.confirm("Are you sure to Logout?");
    confirmation && dispatch(clearToken());
  };
  // const ActiveMenu = useSelector((state) => state.ActiveMenu.value);
  // console.log(ActiveMenu);
  const navigate = useNavigate();

  const handleClick = (menu) => {
    // dispatch(setActiveMenu({ value: menu }));
    navigate("/" + menu);
  };

  const Menu_user = [
    { title: "Assigned", id: "user_assigned", icone: <ContactPage /> },
    { title: "Observed", id: "user_observer", icone: <Visibility /> },
    { title: "Request", id: "user_applicant", icone: <PostAdd /> },
  ];
  const Menu_group = [
    { title: "Assigned", id: "groupsAssigned", icone: <ContactPage /> },
    { title: "Observed", id: "groupsObserver", icone: <Visibility /> },
    { title: "Request", id: "groupsApplicant", icone: <PostAdd /> },
  ];

  const Theme = useSelector((state) => state.theme.value);

  return (
    <>
      <nav
        className={`${active ? "active" : "inactive"} ${
          Theme == "light" ? "light" : "dark"
        }`}
      >
        <ul>
          <h3>USER</h3>
          {Menu_user.map((item, index) => (
            <li
              key={index}
              className={menu == item.id ? "active_menu" : ""}
              onClick={() => handleClick(item.id)}
            >
              {item.icone}
              <a className="nav-items">{item.title}</a>
            </li>
          ))}

          <hr style={{ margin: "1rem 0" }} />
          <h3>GROUP</h3>
          {Menu_group.map((item, index) => (
            <li
              key={index}
              className={menu == item.id ? "active_menu" : ""}
              onClick={() => handleClick(item.id)}
            >
              {item.icone}
              <a className="nav-items">{item.title}</a>
            </li>
          ))}
        </ul>
        <ul>
          <li onClick={handleLogout}>
            <Logout />
            <a className="nav-items">Logout</a>
          </li>
        </ul>
      </nav>
    </>
  );
}

export default Menu;
