import React, { useCallback, useEffect, useState } from "react";
import ExpandLessOutlinedIcon from "@mui/icons-material/ExpandLessOutlined";
import BadgeCircle from "../../components/BadgeCircle/BadgeCircle";
import { formatDate, renderContent, stringAvatar } from "../../services/Utils";
import "./TicketCard.scss";
import { Link } from "react-router-dom";
import AvatarUnique from "../AvatarUnique/AvatarUnique";
import { ListAlt, PersonAdd, PersonRemove } from "@mui/icons-material";
import {
  Avatar,
  Box,
  Breadcrumbs,
  Modal,
  Tooltip,
  Typography,
} from "@mui/material";
import { useSelector } from "react-redux";
import jwt_decode from "jwt-decode";
import { url } from "../../urlApi";
import Breadcrumb from "../Breadcrumb/Breadcrumb";

const TicketCard = ({ item, rendered, setRendered }) => {
  const [data, setData] = useState(item);

  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  
  const [openObserver, setOpenObserver] = React.useState(false);
  const handleOpenObserver = () => setOpenObserver(true);
  const handleCloseObserver = () => setOpenObserver(false);

  const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 400,
    bgcolor: "background.paper",
    border: "2px solid #000",
    borderRadius: "10px",
    boxShadow: 24,
    p: 4,
  };

  const [isOpen, setIsOpen] = useState(false);
  const token = useSelector((state) => state.auth.token);
  const decodJwt = jwt_decode(token);

  const toggleDropdown = () => {
    setIsOpen(!isOpen);
  };

  const BadgeColor = (status) => {
    switch (status) {
      case "in progress":
        return "orange";
      case "new":
        return "green";
      case "closed":
        return "lightgray";

      default:
        break;
    }
  };
  // console.log(data);

  const assigned = [];
  if (data && data.assigned) {
    data.assigned.forEach((element) => {
      assigned.push(element.id);
    });
  }

  const applicant = [];
  if (data && data.applicant) {
    data.applicant.forEach((element) => {
      applicant.push(element.id);
    });
  }
  // console.log(assigned);

  const observer = [];
  if (data && data.observer) {
    data.observer.forEach((element) => {
      observer.push(element.id);
    });
  }

  // console.log(JSON.stringify(data));

  const handleRemoveElement = async (e, item, type) => {
    e.preventDefault();
    const dataFilteredAssigned = item.filter((elm) => elm.id !== decodJwt.id);
    const dataFilteredObservered = item.filter((elm) => elm.id !== decodJwt.id);
    console.log(dataFilteredAssigned);
    const listAssigned = {};
    const listObserver = {};

    dataFilteredAssigned.forEach((element) => {
      listAssigned[element.id] = "api/users/" + element.id;
    });

    dataFilteredObservered.forEach((element) => {
      listObserver[element.id] = "api/users/" + element.id;
    });
    // console.log(listObserver);

    const response = await fetch(url.ticket_put + "/" + data.id, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + token,
      },
      body: JSON.stringify(
        type == "assigned"
          ? { assigned: listAssigned }
          : { observer: listObserver }
      ),
    });
    // console.log(response);
    if (response.ok) {
      const updatedData =
        type == "assigned"
          ? { ...data, assigned: dataFilteredAssigned }
          : { ...data, observer: dataFilteredObservered };
      setData(updatedData);
    } else {
      console.log("error:" + response);
    }
  };

  const handleAddElement = async (e, item, type) => {
    e.preventDefault();
    const listUser = {};

    item.forEach((element) => {
      listUser[element.id] = "api/users/" + element.id;
    });

    listUser[decodJwt.id] = "/api/users/" + decodJwt.id;

    const response = await fetch(url.ticket_put + "/" + data.id, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + token,
      },
      body: JSON.stringify(
        type == "assigned" ? { assigned: listUser } : { observer: listUser }
      ),
    });
    // console.log(response);
    if (response.ok) {
      setRendered(!rendered);
    } else {
      console.log("error:" + response);
    }
  };

  const menus = [
    { id: 1, type: "user", label: "User" },
    { id: 2, type: "group", label: "Group" },
  ];
  const menusObserver = [
    { id: 1, type: "user", label: "User" },
    { id: 2, type: "group", label: "Group" },
  ];

  const [type, setType] = useState("user");
  const [typeObserver, setTypeObserver] = useState("user");

  return (
    <div className={`item ${isOpen ? "open" : "closed"}`}>
      <div className="right">
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <div className="id_status">
            <Link to={`/followUp/ticket/${data.id}`}>
              <h2 className="id">{data.id}</h2>
            </Link>
          </div>
          <span className="date">{formatDate(data.CreatedAt)}</span>
        </div>
        <div style={{ display: "flex", alignItems: "center", gap: "2rem" }}>
          <span style={{ display: "flex", alignItems: "center" }}>
            <BadgeCircle bg={BadgeColor(data.status)} />
            {data.status}
          </span>
          {isOpen ? (
            <ExpandLessOutlinedIcon
              fontSize="large"
              style={{ transition: "transform 0.3s ease-in-out" }}
              onClick={toggleDropdown}
            />
          ) : (
            <ExpandLessOutlinedIcon
              fontSize="large"
              style={{
                transition: "transform 0.3s ease-in-out",
                transform: "rotate(180deg)",
              }}
              onClick={toggleDropdown}
            />
          )}
        </div>
      </div>
      <hr />
      <div className={`center ${isOpen ? "open" : "closed"}`}>
        <h2>{data.title}</h2>
        <div dangerouslySetInnerHTML={renderContent(data.description)} />
      </div>
      <hr />
      <div className={`left ${isOpen ? "open" : "closed"}`}>
        <div className="part demandeur">
          {data.applicant && (
            <>
              <div style={{ display: "flex", flexDirection: "column" }}>
                <p>applicant</p>
                <div style={{ display: "flex" }}>
                  <AvatarUnique item={data.applicant} type={"applicant"} />
                  <AvatarUnique
                    item={data.groupsApplicant}
                    type={"groupsApplicant"}
                  />
                </div>
              </div>
            </>
          )}
        </div>
        <div className="part demandeur">
          {data.assigned && (
            <>
              <div style={{ display: "flex", flexDirection: "column" }}>
                <div
                  style={{ display: "flex", alignItems: "center", gap: "5px" }}
                >
                  <p>assigned</p>
                  <Tooltip title="assigned to me">
                    {assigned.includes(decodJwt.id) ? (
                      <PersonRemove
                        sx={{ cursor: "pointer" }}
                        fontSize="small"
                        color="secondary"
                        onClick={(e) =>
                          handleRemoveElement(e, data.assigned, "assigned")
                        }
                      />
                    ) : (
                      <PersonAdd
                        sx={{ cursor: "pointer" }}
                        fontSize="small"
                        color="secondary"
                        onClick={(e) =>
                          handleAddElement(e, data.assigned, "assigned")
                        }
                      />
                    )}
                  </Tooltip>
                  {
                    <>
                      <Tooltip title="list assigned">
                        <ListAlt
                          sx={{ cursor: "pointer" }}
                          fontSize="small"
                          color="secondary"
                          onClick={handleOpen}
                        />
                      </Tooltip>
                      <Modal
                        open={open}
                        onClose={handleClose}
                        aria-labelledby="modal-modal-title"
                        aria-describedby="modal-modal-description"
                      >
                        <Box sx={style}>
                          <Breadcrumb
                            menus={menus}
                            type={type}
                            setType={setType}
                          />
                          <Typography
                            id="modal-modal-title"
                            variant="h6"
                            component="h2"
                          >
                            {type == "user"
                              ? "User assigned"
                              : "Group assigned"}
                          </Typography>
                        
                            {type == "user" &&
                              data.assigned.map((elm, i) => (
                                <div
                                  key={i}
                                  style={{
                                    display: "flex",
                                    alignItems: "center",
                                    gap: "10px",
                                  }}
                                >
                                  <Avatar
                                    style={{
                                      width: "32px",
                                      height: "32px",
                                      fontSize: "16px",
                                      color: "black",
                                    }}
                                    {...stringAvatar(
                                      elm.firstName + " " + elm.lastName
                                    )}
                                    variant={"circle"}
                                  />
                                  <Typography key={i}>{elm.email}</Typography>
                                </div>
                              ))}
                            {type == "group" &&
                              data.groupsAssigned.map((elm, i) => (
                                <div
                                  key={i}
                                  style={{
                                    display: "flex",
                                    alignItems: "center",
                                    gap: "10px",
                                  }}
                                >
                                  <Avatar
                                    style={{
                                      width: "32px",
                                      height: "32px",
                                      fontSize: "16px",
                                      color: "black",
                                    }}
                                    {...stringAvatar(
                                      elm.name + " " + elm.name,
                                      "group"
                                    )}
                                    variant={"rounded"}
                                  />
                                  <Typography key={i}>
                                    Group {elm.name}
                                  </Typography>
                                </div>
                              ))}
                         
                        </Box>
                      </Modal>
                    </>
                  }
                </div>
                <div style={{ display: "flex" }}>
                  <AvatarUnique item={data.assigned} type={"applicant"} />
                  <AvatarUnique
                    item={data.groupsAssigned}
                    type={"groupsApplicant"}
                  />
                </div>
              </div>
            </>
          )}
        </div>
        <div className="part demandeur">
          {data.observer && (
            <>
              <div style={{ display: "flex", flexDirection: "column" }}>
                <div
                  style={{ display: "flex", alignItems: "center", gap: "5px" }}
                >
                  <p>observer</p>
                  <Tooltip title="observed by me">
                    {observer.includes(decodJwt.id) ? (
                      <PersonRemove
                        sx={{ cursor: "pointer" }}
                        fontSize="small"
                        color="secondary"
                        onClick={(e) =>
                          handleRemoveElement(e, data.observer, "observer")
                        }
                      />
                    ) : (
                      <PersonAdd
                        sx={{ cursor: "pointer" }}
                        fontSize="small"
                        color="secondary"
                        onClick={(e) =>
                          handleAddElement(e, data.observer, "observer")
                        }
                      />
                    )}
                  </Tooltip>
                  {
                    <>
                      <Tooltip title="list observer">
                        <ListAlt
                          sx={{ cursor: "pointer" }}
                          fontSize="small"
                          color="secondary"
                          onClick={handleOpenObserver}
                        />
                      </Tooltip>
                      <Modal
                        open={openObserver}
                        onClose={handleCloseObserver}
                        aria-labelledby="modal-modal-title-observer"
                        aria-describedby="modal-modal-description-observer"
                      >
                        <Box sx={style}>
                          <Breadcrumb
                            menus={menusObserver}
                            type={typeObserver}
                            setType={setTypeObserver}
                          />
                          <Typography
                            id="modal-modal-title-observer"
                            variant="h6"
                            component="h2"
                          >
                            {typeObserver == "user"
                              ? "User observer"
                              : "Group observer"}
                          </Typography>
                          
                            {typeObserver == "user" &&
                              data.observer.map((elm, i) => (
                                <div
                                  key={i}
                                  style={{
                                    display: "flex",
                                    alignItems: "center",
                                    gap: "10px",
                                  }}
                                >
                                  <Avatar
                                    style={{
                                      width: "32px",
                                      height: "32px",
                                      fontSize: "16px",
                                      color: "black",
                                    }}
                                    {...stringAvatar(
                                      elm.firstName + " " + elm.lastName
                                    )}
                                    variant={"circle"}
                                  />
                                  <Typography key={i}>{elm.email}</Typography>
                                </div>
                              ))}
                            {typeObserver == "group" &&
                              data.groupsObserver.map((elm, i) => (
                                <div
                                  key={i}
                                  style={{
                                    display: "flex",
                                    alignItems: "center",
                                    gap: "10px",
                                  }}
                                >
                                  <Avatar
                                    style={{
                                      width: "32px",
                                      height: "32px",
                                      fontSize: "16px",
                                      color: "black",
                                    }}
                                    {...stringAvatar(
                                      elm.name + " " + elm.name,
                                      "group"
                                    )}
                                    variant={"rounded"}
                                  />
                                  <Typography key={i}>
                                    Group {elm.name}
                                  </Typography>
                                </div>
                              ))}
                         
                        </Box>
                      </Modal>
                    </>
                  }
                </div>
                <div style={{ display: "flex" }}>
                  <AvatarUnique item={data.observer} type={"applicant"} />
                  <AvatarUnique
                    item={data.groupsObserver}
                    type={"groupsApplicant"}
                  />
                </div>
              </div>
            </>
          )}
        </div>
        <div className="part suivi">
          {data.followUps && (
            <div>
              <p>
                follow up
                <span style={{ color: "violet" }}>
                  ({data.followUps.length})
                </span>
              </p>
              <AvatarUnique item={data.followUps} type={"followup"} />
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default TicketCard;
