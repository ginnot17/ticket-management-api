import React from "react";
import { formatDate, renderContent } from "../../services/Utils";
import { Avatar, Tooltip } from "@mui/material";
import { PersonOutline, RestoreFromTrashOutlined } from "@mui/icons-material";
import { url } from "../../urlApi";
import { useSelector } from "react-redux";

function DeleteChatApplicant({ item, decodJwt,setDeleteClicked,deleteClicked }) {
  const token = useSelector((state) => state.auth.token);
  const handleDelete = async (e, id) => {
    e.preventDefault();
    try {
      const response = await fetch(url.Put_deleted_followup + "/" + id, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + token,
        },
        body: JSON.stringify({
          deleted: 0,
        }),
      });
      if (response.ok) {
        console.log("deleted successfully:" + response);
      } else {
        console.log("deleted error:" + response);
      }
      setDeleteClicked(!deleteClicked);
    } catch (error) {
      console.error("Error catched:" + error);
    }
  };
  return (
    <div
      style={{
        display: "flex",
        // justifyContent: "start",
        paddingLeft: "10px",
        flexDirection: "column",
      }}
    >
      <p
        style={{
          margin: "0 auto",
          fontSize: "10px",
          opacity: "0.5",
        }}
      >
        {formatDate(item.CreatedAt)}
      </p>
      <div style={{ display: "flex" }}>
        <Avatar
          style={{
            width: "30px",
            height: "30px",
            fontSize: "12px",
            color: "black",
          }}
        >
          <PersonOutline />
        </Avatar>

        <div key={item.id} className="chat-bubble deleted">
          <div
            dangerouslySetInnerHTML={renderContent(
              "content deleted by its author"
            )}
          />
        </div>
        {item.users.id == decodJwt.id && (
          <div style={{ margin: "auto 0" }}>
            <Tooltip title="restore">
              <RestoreFromTrashOutlined
                fontSize="small"
                className="FollowUpIcon"
                onClick={(e) => handleDelete(e, item.id)}
              />
            </Tooltip>
          </div>
        )}
      </div>
    </div>
  );
}

export default DeleteChatApplicant;
