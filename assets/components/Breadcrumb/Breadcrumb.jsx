import React, { memo } from "react";
import "./Breadcrumb.scss";

const Breadcrumb = memo(({ menus, type, setType }) => {
  const handleLinkClick = (link) => {
    setType(link);
  };
  return (
    <p className="breadCrumbCountainer">
      {menus.map((menu, index) => (
        <React.Fragment key={menu.id}>
          <a
            className={type === menu.type ? "active_ticket" : ""}
            onClick={() => handleLinkClick(menu.type)}
          >
            {menu.label}
          </a>
          {index < menus.length - 1 && <span>/</span>}
        </React.Fragment>
      ))}
    </p>
  );
});

export default Breadcrumb;
