import React, { memo } from "react";
import Avatar from "@mui/material/Avatar";
import { stringAvatar } from "../../services/Utils";
import { AvatarGroup, Tooltip } from "@mui/material";

const AvatarUnique = memo(function AvatarUnique({ item, type }) {
  //   console.log(item);
  var variant = "";
  const uniqueAvatars = {};
  if (item) {
    item.map((content, i) => {
      var userName = "";
      switch (type) {
        case "applicant":
          userName = content.firstName + " " + content.lastName;
          variant = "user";
          break;
        case "followup":
          userName = content.users.firstName + " " + content.users.lastName;
          variant = "user";
          break;
        case "groupsApplicant":
          userName = content.name + " " + content.name;
          variant = "group";
          break;

        default:
          break;
      }
      if (!uniqueAvatars[userName]) {
        uniqueAvatars[userName] = userName;
      }
    });
  }

  const avatarElements = Object.values(uniqueAvatars);
  // console.log('ici');

  return (
    <div style={{ display: "flex", flexWrap: "wrap" }}>
      <AvatarGroup max={3}
      sx={{
        '& .MuiAvatar-root': { width: "32px", height: "32px", fontSize: 16,marginTop:"10px" },
      }}
      >
        {avatarElements.map((user, index) => (
          <Tooltip
            key={index}
            title={`${
              variant == "group" ? "group " + user.split(" ")[0] : user
            }`}
          >
            <Avatar
              style={{
                marginTop: "10px",
                width: "32px",
                height: "32px",
                fontSize: "16px",
                color: "black",
              }}
              {...(variant === "user"
                ? stringAvatar(user)
                : stringAvatar(user, "group"))}
              variant={variant === "group" ? "rounded" : "circle"}
            />
          </Tooltip>
        ))}
      </AvatarGroup>
    </div>
  );
});

export default AvatarUnique;
