import { ThemeProvider } from "@emotion/react";
import { Avatar, CssBaseline, Skeleton } from "@mui/material";
import React from "react";

function SkeletonFollowUp() {
  return (
    <div
      style={{
        display: "flex",
        gap: "1rem",
        flexDirection: "column",
      }}
    >
      {Array.from({ length: 10 }).map((_, i) => (
        <div key={i}>
          {i % 2 === 0 ? (
            <div
              style={{
                display: "flex",
                gap: "5px",
                alignItems: "flex-start",
                marginLeft: "1rem",
              }}
            >
              <Skeleton variant="circular">
                <Avatar />
              </Skeleton>
              <Skeleton
                variant="rounded"
                sx={{ width: "50%", height: "100px", float: "left" }}
              />
            </div>
          ) : (
            <div
              style={{
                display: "flex",
                gap: "5px",
                alignItems: "flex-start",
                flexDirection: "row-reverse",
                padding: "10px",
              }}
            >
              <Skeleton variant="circular">
                <Avatar />
              </Skeleton>
              <Skeleton
                variant="rounded"
                sx={{ width: "50%", height: "100px", float: "left" }}
              />
            </div>
          )}
        </div>
      ))}
    </div>
  );
}

export default SkeletonFollowUp;
