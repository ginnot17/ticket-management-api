import React from 'react'
import './BdgeCircle.scss'

function BadgeCircle({ bg, size = "default" }) {
  // console.log(bg);
  return (
    <span className={`${size == "default" ? 'badgeCircle' : size}`} style={{ backgroundColor: `${bg}` }}></span>
  )
}

export default BadgeCircle