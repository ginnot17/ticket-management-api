import React, { memo } from "react";
import "./BoxGradiant.scss";

const BoxGradiant = memo(({ title, stat }) => {
  return (
    <div className="box">
      <h1>{title}</h1>
      <div className="stat">
        <h2>{stat}</h2>
      </div>
    </div>
  );
});

export default BoxGradiant;
