
export const url =
{

    "auth": '/auth', // POST
    "register": '/api/users', // POST
    "group": '/api/groups', // GET
    "ticket_user": '/api/tickets/user', // GET
    "ticket_id": '/api/tickets', // GET
    "ticket_group": '/api/tickets/group', // GET
    "ticket_followUp": '/api/followUp/ticket', // GET
    "ticket_create": '/api/tickets', // POST
    "ticket_put": '/api/tickets', // PUT
    "followup_create": '/api/followUp/ticket', // POST
    "Put_attachement": "/api/attachements/",// PUT
    "Put_deleted_followup": "/api/follow_ups", // PUT
    "Put_followup": "/api/follow_ups" // PUT

}

