import { url } from "../urlApi";


const TestResponse = async (response, seterror = () => { }, setData = () => { },setLoading) => {
    // console.log(response);
    setLoading(true);
    if (response.ok) {
        const data_ = await response.json();
        const data = await data_["hydra:member"];
        if (data) {
            setData(data);
            seterror("");
        } else {
            setData(data_);
            seterror("");
        }
        setLoading(false)

    } else {
        if (response.status) {
            switch (response.status) {
                case 500:
                    seterror("API:Error 500 Internal Server Error");
                    break;
                case 404:
                    seterror("API:Error 404 Not Found");
                    break;
                default:
                    break;
            }
        }
        const errorText = await response.json();
        console.log(errorText);
        if (errorText && errorText.code == '401') {
            seterror(errorText.message)
        }
        setLoading(false)
    }
}

const fetchListTicketByUser = async (token, role_user = '', type = '', seterror, setData,setLoading) => {
    try {
        const response = await fetch(`${url.ticket_user}?${role_user !== '' ? `user_ticket_role=${role_user.replace(/user_/g, "")}` : ''}${type !== '' ? `&type=${type}` : ''}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + token,
            },
        });
        TestResponse(response, seterror, setData,setLoading)

    } catch (error) {
        console.log("error:" + error);
    }
};
const fetchListTicketByGroup = async (token, role_group = '', type = '', seterror, setData,setLoading) => {
    try {
        const response = await fetch(`${url.ticket_group}?${role_group !== '' ? `group_ticket_role=${role_group}` : ''}${type !== '' ? `&type=${type}` : ''}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + token,
            },
        });
        TestResponse(response, seterror, setData,setLoading)

    } catch (error) {
        console.log("error:" + error);
    }
};




export { fetchListTicketByUser,fetchListTicketByGroup }