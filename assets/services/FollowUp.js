import { url } from "../urlApi";

const fetchListByIdTicket = async (setDataFollowUp, id, token, setError, setLoading) => {
    try {
        setLoading(true);
        setDataFollowUp([])
        const response = await fetch(url.ticket_followUp + "?id=" + id, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                Authorization: "Bearer " + token,
            },
        });

        // console.log("response: " + response);
        if (response.ok) {
            const data = await response.json();
            // console.log(data);
            setDataFollowUp(data);
            setError("")

        } else {
            console.log(response.status);
            if (response.status) {
                switch (response.status) {
                    case 500:
                        setError("API:Error 500 Internal Server Error");
                        return;
                    case 404:
                        setError("API:Error 404 Not Found");
                        return;
                    case 400:
                        const errorText = await response.json();
                        console.log(errorText.detail);
                        setError(errorText.detail);
                        return;
                    default:
                        break;
                }
            }
            const errorText = await response.json();
            setError(errorText.message)
            console.log(errorText);
        }
        setLoading(false);

    } catch (error) {
        console.log("catch error: " + error);
        setLoading(false);
    }
};

async function AddFollowUp(id, url, token, setDataFollowUp, setError, editorDesc, setEditorDesc, setLoading, status,dataTicketInfo,idUser) {
    try {
        const regex = /^(<p><br><\/p>|<p>\s*<\/p>)+$/;
        setLoading(true);
        const response = await fetch(url.followup_create + "?id=" + id, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Authorization: "Bearer " + token,
            },
            body: JSON.stringify({
                content: regex.test(editorDesc) ? "" : editorDesc,
                deleted: 0,
                solution: status == "closed" && (dataTicketInfo.applicant.includes("/api/users/"+idUser) || dataTicketInfo.assigned.includes("/api/users/"+idUser)) ? 1 : 0
            }),
        });
        // console.log(response);
        if (response.ok) {
            const data = await response.json();
            // console.log(data.id);
            setDataFollowUp(val => [...val, data]);
            setEditorDesc("");
            setLoading(false);
            // console.log(data);
            return data;
        } else {
            // console.log(response.status);
            if (response.status) {
                switch (response.status) {
                    case 500:
                        setError("API:Error 500 Internal Server Error");
                        setLoading(false);
                        return;
                    case 404:
                        setError("API:Error 404 Not Found");
                        setLoading(false);
                        return;
                    case 400:
                        const errorText = await response.json();
                        // console.log(errorText);
                        if (errorText["hydra:description"]) {
                            setError(errorText["hydra:description"]);
                        } else {
                            setError(errorText.detail);
                        }
                        setLoading(false);
                        return;
                    default:
                        break;
                }
            }
            const errorText = await response.json();
            setError(errorText.message);
            setLoading(false);
            console.log(errorText);
        }
        setEditorDesc("");
        setLoading(false);
    } catch (error) {
        console.error(error);
    }



}

const fetchStatusTicket = async (id,dispatch,setStatusTicket,token,setStatus,statusTicket,setDataTicketInfo) => {
    try {
      const response = await fetch(url.ticket_id + "/" + id, {
        method: "GET",
        headers: {
          Authorization: "Bearer " + token,
          "Content-Type": "application/json",
        },
      });

      if (response.ok) {
        const data = await response.json();
        // console.log(data);
        dispatch(setStatusTicket({ statusTicket: data.status }));
        setStatus(statusTicket);
        setDataTicketInfo(data)
      } else {
        console.error("erreur for get ticket id:" + id);
      }
    } catch (error) {
      console.log("catch error:" + error);
    }
  };

export { fetchListByIdTicket, AddFollowUp,fetchStatusTicket }