import DOMPurify from 'dompurify';


const formatDate = (dateString) => {
  const options = {
    year: 'numeric',
    month: '2-digit',
    day: '2-digit',
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit'
  };
  return new Date(dateString).toLocaleDateString('en-US', options); // Format anglais (MM/DD/YYYY HH:MM:SS)
};

function stringToColor(string) {
  let hash = 0;
  let i;
  for (i = 0; i < string.length; i += 1) {
    hash = string.charCodeAt(i) + ((hash << 5) - hash);
  }

  let color = "#";

  for (i = 0; i < 3; i += 1) {
    const value = (hash >> (i * 8)) & 0xff;
    color += `00${value.toString(16)}`.slice(-2);
  }

  return color;
}

function stringAvatar(name, type = 'user') {
  // console.log(name);
  let avatarText = '';
  if (type === 'user') {
    avatarText = `${name.split(" ")[0][0]}${name.split(" ")[1][0]}`;
  } else {
    avatarText = `${name.split(" ")[0]}`;
  }
  return {
    sx: {
      bgcolor: stringToColor(name),
    },
    children: avatarText,
  };
}

function getCurrentDateAndTime() {
  const currentDate = new Date();

  const year = currentDate.getFullYear();
  const month = ('0' + (currentDate.getMonth() + 1)).slice(-2);
  const day = ('0' + currentDate.getDate()).slice(-2);
  const hours = ('0' + currentDate.getHours()).slice(-2);
  const minutes = ('0' + currentDate.getMinutes()).slice(-2);
  const seconds = ('0' + currentDate.getSeconds()).slice(-2);

  return `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`;
}

function renderContent(htmlContent) {
  const cleanHTML = DOMPurify.sanitize(htmlContent);
  return { __html: cleanHTML };
}

export { formatDate, stringAvatar, getCurrentDateAndTime, renderContent }