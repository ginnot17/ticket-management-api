import { url } from "../urlApi";

const fetchListGroup = async () => {
    try {
        const responseData = await fetch(url.group, {
            method: "GET",
            headers: {
                "content-type": "application/json",
                Accept: "application/json",
            },
        });
        if (responseData.ok) {
            const jsonData = await responseData.json();
            return jsonData;


        } else {
            console.error("Erreur lors de la réponse API :", responseData.status);
        }

    } catch (error) {
        console.log(error);
    }
};

export {fetchListGroup}