import { createSlice } from "@reduxjs/toolkit"

const initialState = {
    statusTicket: ""
}

export const statusTicketSlice = createSlice({
    name: "statusTicket",
    initialState,
    reducers: {
        setStatusTicket: (state, action) => {
            state.statusTicket = action.payload.statusTicket
        }
    }
})

export const { setStatusTicket } = statusTicketSlice.actions
export default statusTicketSlice.reducer