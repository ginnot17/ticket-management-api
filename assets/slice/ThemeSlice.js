import { createSlice } from "@reduxjs/toolkit";

const ThemeSlice = createSlice({
  name: 'theme',
  initialState: {
    value: 'light'
  },
  reducers: {
    setTheme: (state, action) => {
      state.value = action.payload.value;
    }
  }
});

export const { setTheme } = ThemeSlice.actions;
export default ThemeSlice.reducer;
