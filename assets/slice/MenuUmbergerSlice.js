import { createSlice } from '@reduxjs/toolkit'

export const activeUmbergerSlice = createSlice({
  name: 'ActiveUmberger',
  initialState: {
    value: false
  },
  reducers: {
    setActive: state => {
      state.value = !state.value
    },

  }
})

// Action creators are generated for each case reducer function
export const { setActive } = activeUmbergerSlice.actions

export default activeUmbergerSlice.reducer