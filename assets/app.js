/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import React from 'react'
import ReactDOM from 'react-dom/client'
import Root from './Root';
import './styles/app.scss';
import { BrowserRouter, HashRouter } from 'react-router-dom';
import {store,persistor} from './store';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';


ReactDOM.createRoot(document.getElementById('root')).render(
    <>
        <Provider store={store}>
            <PersistGate loading={null} persistor={persistor}>
                <HashRouter >
                    <Root />
                </HashRouter>
            </PersistGate>
        </Provider>
    </>
)

