import { configureStore } from '@reduxjs/toolkit'
import activeUmbergerReducer from './slice/MenuUmbergerSlice'
import authSliceReducer from './slice/AuthSlice'
import ThemeSliceReducer from './slice/ThemeSlice'
import StatusTicketSliceReducer from './slice/StatusTicketSlice'
import { persistStore, persistReducer } from 'redux-persist';
import {
  FLUSH,
  REHYDRATE,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER,
} from 'redux-persist'
import storage from 'redux-persist/lib/storage'; // defaults to localStorage for web


const authPersistConfig = {
  key: 'auth',
  storage,
};
const MenuUmbergerPersistConfig = {
  key: 'auActiveUmbergerth',
  storage,
};
const ThemePersistConfig = {
  key: 'Theme',
  storage,
};
const StatusTicketPersistConfig = {
  key: 'statusTicket',
  storage,
};

const persistedAuthReducer = persistReducer(authPersistConfig, authSliceReducer);
const persistedMenuUmbergerReducer = persistReducer(MenuUmbergerPersistConfig, activeUmbergerReducer);
const persistedThemeReducer = persistReducer(ThemePersistConfig, ThemeSliceReducer);
const persistedStatusTicketReducer = persistReducer(StatusTicketPersistConfig, StatusTicketSliceReducer);

export const store = configureStore({
  reducer: {
    ActiveUmberger: persistedMenuUmbergerReducer,
    auth: persistedAuthReducer,
    theme: persistedThemeReducer,
    statusTicket: persistedStatusTicketReducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
      },
    }),
})

export const persistor = persistStore(store);