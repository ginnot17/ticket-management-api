import React from 'react'
import { Outlet, Route, Routes as Router, Routes } from "react-router-dom"
import Home from "./pages/home/Home"
import Menu from "./components/Menu/Menu"
import Header from "./components/Header/Header"
import Login from './pages/Login/Login'
import Register from './pages/Register/Register'
import PrivateRoute from './components/PrivateRoute'
import { useSelector } from 'react-redux'
import CreateTicket from './pages/CreateTicket.js/CreateTicket'
import FollowUp from './pages/FollowUp/FollowUp'


const Root = (props) => {

    const Layout = () => {
        const Theme = useSelector((state) => state.theme.value);
        return (
            <div className={`main ${Theme == 'light' ? 'light' : 'dark'}`}>
                <Header />
                <div className='container'>
                    <Menu />
                    <Outlet />
                </div>
            </div>
        );
    }
    return (

        <Routes>
            <Route path="/login" element={<Login />} />
            <Route path="/register" element={<Register />} />
            <Route element={<PrivateRoute />}>
                <Route element={<Layout />}>
                    <Route path="/" element={<Home />} />
                    <Route path="/:menu" element={<Home />} />
                    <Route path="/createTicket" element={<CreateTicket />} />
                    <Route path="/followUp/ticket/:id" element={<FollowUp />} />
                </Route>
            </Route>
        </Routes>
    )
}

export default Root