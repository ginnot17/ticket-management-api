import React, { useEffect, useState } from "react";
import "./Home.scss";
import { useSelector } from "react-redux";
import { Link, useParams } from "react-router-dom";
import {
  fetchListTicketByGroup,
  fetchListTicketByUser,
} from "../../services/Ticket";
import { Alert, Button, CircularProgress } from "@mui/material";
import BoxGradiant from "../../components/BoxGradiant/BoxGradiant";
import Breadcrumb from "../../components/Breadcrumb/Breadcrumb";
import TicketCard from "../../components/TicketCard/TicketCard";
import { PlaylistAddOutlined } from "@mui/icons-material";

function Home() {
  const token = useSelector((state) => state.auth.token);
  let { menu } = useParams();
  //   console.log(menu);
  const [type, setType] = useState("incident");
  
  const [dataTicket, setDataTicket] = useState([]);
  const [error, seterror] = useState("");
  const [loading, setLoading] = useState(true);
  const [rendered, setRendered] = useState(true);

  useEffect(() => {
    if (menu) {
      if (menu.split("_")[0] == "user") {
        fetchListTicketByUser(
          token,
          menu,
          type,
          seterror,
          setDataTicket,
          setLoading
        );
      } else {
        fetchListTicketByGroup(
          token,
          menu,
          type,
          seterror,
          setDataTicket,
          setLoading
        );
      }
    }
  }, [token, menu, type, rendered]);

  const statNewTicket = dataTicket.filter((ticket) => ticket.status === "new");
  const statInProgressTicket = dataTicket.filter(
    (ticket) => ticket.status === "in progress"
  );
  const statClosedTicket = dataTicket.filter(
    (ticket) => ticket.status === "closed"
  );

  // console.log("render Home.jsx");
  const menus = [
    { id: 1, type: "incident", label: "Incidents" },
    { id: 2, type: "request", label: "Requests" },
  ];

  function ContainerGradiant() {
    const Theme = useSelector((state) => state.theme.value);
    return (
      <div data-theme={Theme} className="home_stat">
        <BoxGradiant title={"new"} stat={statNewTicket.length} />
        <BoxGradiant title={"in progress"} stat={statInProgressTicket.length} />
        <BoxGradiant title={"closed"} stat={statClosedTicket.length} />
      </div>
    );
  }

  return (
    <div className={`home_container ${error !== "" ? "error" : ""}`}>
      {error !== "" ? (
        <div
          style={{ height: "50px", display: "flex", justifyContent: "center" }}
        >
          <Alert severity="warning">{error} </Alert>
        </div>
      ) : (
        <>
          <div className="home_menu">
            <Breadcrumb
              menus={menus}
              type={type}
              setType={setType}
            />
          </div>

          <div className="home_title">
            <div className="nb_ticket">
              <h1>
                {loading ? (
                  <CircularProgress color="secondary" />
                ) : (
                  dataTicket && dataTicket.length
                )}{" "}
                ticket{" "}
              </h1>
            </div>
            <Link to="/createTicket">
              <Button
                variant="contained"
                color="secondary"
                startIcon={<PlaylistAddOutlined />}
              >
                CREATE
              </Button>
            </Link>
          </div>
          <ContainerGradiant />
          <div className="home_item_container">
            {loading ? (
              <CircularProgress sx={{ margin: "auto" }} color="secondary" />
            ) : (
              dataTicket &&
              dataTicket.map((item, index) => (
                <div key={item.id}>
                  <TicketCard
                    key={index}
                    item={item}
                    setRendered={setRendered}
                    rendered={rendered}
                  />
                </div>
              ))
            )}
          </div>
        </>
      )}
    </div>
  );
}

export default Home;
