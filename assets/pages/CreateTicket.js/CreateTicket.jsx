import React, { useEffect, useRef, useState } from "react";
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";
import "./CreateTicket.scss";
import {
  Button,
  CssBaseline,
  FormControl,
  FormHelperText,
  InputLabel,
  MenuItem,
  Select,
  Skeleton,
  TextField,
  ThemeProvider,
  createTheme,
} from "@mui/material";
import { useSelector } from "react-redux";
import { Send } from "@mui/icons-material";
import { url } from "../../urlApi";
import { fetchListGroup } from "../../services/Groups";
import jwt_decode from "jwt-decode";
import { getCurrentDateAndTime } from "../../services/Utils";
import { useNavigate } from "react-router-dom";

function CreateTicket() {
  const editorRef = useRef();
  const navigate = useNavigate();
  const Theme = useSelector((state) => state.theme.value);
  const token = useSelector((state) => state.auth.token);
  const decodedToken = jwt_decode(token);

  const darkTheme = createTheme({
    palette: {
      mode: Theme,
    },
  });
  const [Title, setTitle] = useState("");
  const [editorDesc, setEditorDesc] = useState("");
  const [loading, setLoading] = useState(true);
  const handleChangeTitle = (e) => {
    setTitle(e.target.value);
  };
  const [groupAssigned, setGroup] = useState("");
  const [dataGroup, setDataGroup] = useState([]);
  useEffect(() => {
    const fetchData = async () => {
      try {
        const data = await fetchListGroup();
        setDataGroup(data);
        // if (data.length > 0) {
        //   setGroup(`/api/groups/${data[0].id}`);
        // }
        setLoading(false);
      } catch (error) {
        console.error("Error fetching data:", error);
        setLoading(false);
      }
    };
    fetchData();
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);
    const form = e.target;
    const formData = new FormData(form);

    const ErrorPersist = {};
    if (groupAssigned == "") {
      ErrorPersist["groups"] = "This value should not be blank.";
      setError(ErrorPersist);
      setLoading(false);
      return;
    }
    try {
      const response = await fetch(url.ticket_create, {
        method: "POST",
        headers: {
          "content-type": "application/json",
          Authorization: "Bearer " + token,
        },
        body: JSON.stringify({
          title: Title,
          description: editorRef.current.value,
          applicant: ["/api/users/" + decodedToken.id],
          groupsAssigned: [groupAssigned],
          groupsApplicant: ["/api/groups/" + decodedToken.idGroup],
          status: "new",
          createdAt: getCurrentDateAndTime(),
          type: dataType,
        }),
      });
      const data = await response.json();

      if (data.violations) {
        data.violations.forEach((element) => {
          ErrorPersist[element.propertyPath] = element.message;
        });
        setError(ErrorPersist);
        console.log(data.violations);
      } else {
        console.log(data);
        setError({});
        navigate("/groupsApplicant");
      }
      setLoading(false);
    } catch (error) {
      console.log("catch error:" + error);
      setLoading(false);
    }
  };

  const [error_field, setError] = useState({
    title: "",
    type: "",
    groups: "",
  });

  const [dataType, setDataType] = useState("");

  const choiceType = [
    { id: "1", name: "incident" },
    { id: "2", name: "request" },
  ];

  return (
    <form onSubmit={handleSubmit} className="container_create_ticket">
      <ThemeProvider theme={darkTheme}>
        <CssBaseline />
        {loading ? (
          <>
            <Skeleton
              style={{ margin: "1rem auto" }}
              variant="rectangular"
              width="100%"
              height={50}
            />
          </>
        ) : (
          <TextField
            error={error_field.title ? true : false}
            helperText={(error_field.title && `${error_field.title}`) || ""}
            onChange={handleChangeTitle}
            label="Title"
            variant="outlined"
            color="secondary"
            className="fieldTitle"
            value={Title}
            name="title"
          />
        )}
        {loading ? (
          <Skeleton
            sx={{ height: "3rem" }}
            animation="wave"
            variant="rectangular"
          />
        ) : (
          <FormControl error={error_field.groups ? true : false}>
            <InputLabel color="secondary" id="demo-simple-select-label">
              Group Assigned
            </InputLabel>
            <Select
              color="secondary"
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={groupAssigned}
              label="Group Assigned"
              onChange={(e) => setGroup(e.target.value)}
            >
              {dataGroup &&
                dataGroup.map((value) => (
                  <MenuItem
                    value={`/api/groups/${value.id}`}
                    key={`${value.id}`}
                  >{`${value.name}`}</MenuItem>
                ))}
            </Select>
            {error_field.groups && (
              <FormHelperText>{error_field.groups}</FormHelperText>
            )}
          </FormControl>
        )}
        {loading ? (
          <Skeleton
            sx={{ height: "3rem" }}
            animation="wave"
            variant="rectangular"
          />
        ) : (
          <FormControl error={error_field.type ? true : false}>
            <InputLabel color="secondary" id="demo-simple-select-label">
              Type ticket
            </InputLabel>
            <Select
              color="secondary"
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={dataType}
              label="Type ticket"
              onChange={(e) => setDataType(e.target.value)}
            >
              {choiceType &&
                choiceType.map((value) => (
                  <MenuItem
                    value={`${value.name}`}
                    key={`${value.id}`}
                  >{`${value.name}`}</MenuItem>
                ))}
            </Select>
            {error_field.type && (
              <FormHelperText>{error_field.type}</FormHelperText>
            )}
          </FormControl>
        )}

        {loading ? (
          <>
            <Skeleton
              style={{ margin: "1rem auto" }}
              variant="rectangular"
              width="100%"
              height={200}
            />
          </>
        ) : (
          <FormControl>
            <InputLabel>Description</InputLabel>
            <br />
            <br />
            <div style={{ marginBottom: "2rem" }}>
              <ReactQuill className="textareaField" ref={editorRef} />
            </div>
          </FormControl>
        )}
      </ThemeProvider>
      {loading ? (
        <ThemeProvider theme={darkTheme}>
          <CssBaseline />
          <Skeleton
            style={{ margin: "1rem auto" }}
            variant="rectangular"
            width="30%"
            height={50}
          />
        </ThemeProvider>
      ) : (
        <Button
          style={{ width: "30%", margin: "3rem auto" }}
          variant="contained"
          color="secondary"
          endIcon={<Send />}
          type="submit"
        >
          SAVE
        </Button>
      )}
    </form>
  );
}

export default CreateTicket;
