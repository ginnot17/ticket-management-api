import React, { useEffect, useState } from "react";
import "./Login.scss";
import {
  Alert,
  Button,
  IconButton,
  InputAdornment,
  TextField,
} from "@mui/material";
import { Send, Visibility, VisibilityOff } from "@mui/icons-material";
import { useDispatch, useSelector } from "react-redux";
import { url } from "../../urlApi";
import { setToken } from "../../slice/AuthSlice";
import { Link, Navigate, useNavigate } from "react-router-dom";
import { ThemeProvider, createTheme } from "@mui/material/styles";
import CssBaseline from "@mui/material/CssBaseline";

function Login() {
  const Theme = useSelector((state) => state.theme.value);

  const darkTheme = createTheme({
    palette: {
      mode: Theme,
    },
  });
  const [error, setError] = useState("");
  const [showPassword, setShowPassword] = React.useState(false);

  const handleClickShowPassword = () => setShowPassword((show) => !show);

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  useEffect(() => {
    if (error !== "") {
      const timeoutId = setTimeout(() => {
        setError("");
      }, 3000);

      return () => clearTimeout(timeoutId);
    }
  }, [error]);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const handleLogin = async (e) => {
    e.preventDefault();
    try {
      const response = await fetch(url.auth, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          email: email,
          password: password,
        }),
      });

      const data = await response.json();

      if (response.ok) {
        // console.log(data.token);
        dispatch(setToken({ token: data.token }));
        navigate("/user_assigned");
      } else {
        console.log("Error logging in:", data.message);
        setError(data.message);
      }
    } catch (error) {
      console.error("Error logging in:", error);
    }
  };

  return (
    <div className="LoginContainer">
      <div className="formCountainer">
        <h1>Sign In</h1>
        {error !== "" && <Alert severity="warning">{error}</Alert>}

        <form action="">
          <ThemeProvider theme={darkTheme}>
            <CssBaseline />
            <TextField
              color="secondary"
              onChange={(e) => setEmail(e.target.value)}
              label="Email"
              variant="outlined"
            />
            <TextField
              color="secondary"
              className="textField"
              onChange={(e) => setPassword(e.target.value)}
              label="Password"
              variant="outlined"
              type={showPassword ? "text" : "password"}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={handleClickShowPassword}
                      onMouseDown={handleMouseDownPassword}
                      edge="end"
                    >
                      {showPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  </InputAdornment>
                ),
              }}
            />
          </ThemeProvider>
          <Button
            type="submit"
            variant="contained"
            onClick={handleLogin}
            color="secondary"
            endIcon={<Send />}
          >
            Login
          </Button>
          <p>
            Don't have an account{" "}
            <Link className="signUp" to="/register">
              Sign Up
            </Link>
          </p>
        </form>
      </div>
    </div>
  );
}

export default Login;
