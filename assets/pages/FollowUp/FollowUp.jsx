import React, { useEffect, useMemo, useRef, useState } from "react";
import "./FollowUp.scss";
import ReactQuill from "react-quill";
import ExpandLessOutlinedIcon from "@mui/icons-material/ExpandLessOutlined";
import {
  Alert,
  Button,
  CssBaseline,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  ThemeProvider,
  createTheme,
} from "@mui/material";
import { HighlightOff, Send } from "@mui/icons-material";
import { useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import jwt_decode from "jwt-decode";
import {
  AddFollowUp,
  fetchListByIdTicket,
  fetchStatusTicket,
} from "../../services/FollowUp";
import { url } from "../../urlApi";
import ChatBubbleApplicant from "../../components/ChatBubbleApplicant/ChatBubbleApplicant";
import ChatBubbleFollowUp from "../../components/ChatBubbleFollowUp/ChatBubbleFollowUp";
import DeleteChatApplicant from "../../components/ChatBubbleApplicant/DeleteChatApplicant";
import DeleteChatFollowUp from "../../components/ChatBubbleFollowUp/DeleteChatFollowUp";
import SkeletonFollowUp from "../../components/Skeleton/SkeletonFollowUp";
import BadgeCircle from "../../components/BadgeCircle/BadgeCircle";
import { setStatusTicket } from "../../slice/StatusTicketSlice";
import { renderContent } from "../../services/Utils";

function FollowUp() {
  const Theme = useSelector((state) => state.theme.value);
  const dispatch = useDispatch();
  const statusTicket = useSelector((state) => state.statusTicket.statusTicket);

  const darkTheme = createTheme({
    palette: {
      mode: Theme,
    },
  });
  const quillRef = useRef(null);
  const { id } = useParams(); // id Ticket
  const token = useSelector((state) => state.auth.token);
  const decodJwt = jwt_decode(token);
  const [attachement, setAttachement] = useState([]);
  const [error, setError] = useState("");
  // console.log("params id_ticket: " + id);
  const [editorDesc, setEditorDesc] = useState("");
  const [dataFollowUp, setDataFollowUp] = useState([]);
  const [dataTicketInfo, setDataTicketInfo] = useState([]);
  const [isOpen, setIsOpen] = useState(true);
  const [isEdit, setIsEdit] = useState(false);
  const [idFollowUp, setIdFollowUp] = useState("");
  const [loading, setLoading] = useState(true);
  const [status, setStatus] = useState("");

  const optionStatusTicket = [
    {
      id: 1,
      status: "in progress",
      color: "orange",
      label: "In progress",
    },
    {
      id: 2,
      status: "closed",
      color: "lightgray",
      label: "Closed",
    },
    {
      id: 3,
      status: "new",
      color: "green",
      label: "New",
    },
  ];

  const toggleDropdown = () => {
    setIsOpen(!isOpen);
  };
  const [deleteClicked, setDeleteClicked] = useState(false);
  useEffect(() => {
    fetchListByIdTicket(setDataFollowUp, id, token, setError, setLoading);
  }, [id, deleteClicked]);

  useEffect(() => {
    fetchStatusTicket(
      id,
      dispatch,
      setStatusTicket,
      token,
      setStatus,
      statusTicket,
      setDataTicketInfo
    );
  }, [statusTicket, id]);

  useEffect(() => {
    if (error !== "") {
      const timeoutId = setTimeout(() => {
        setError("");
      }, 3000);

      return () => clearTimeout(timeoutId);
    }
  }, [error]);

  // console.log(dataTicketInfo);

  const handleAddFollowUp = async (e) => {
    e.preventDefault();

    if (
      !dataTicketInfo.observer.includes("/api/users/" + decodJwt.id) &&
      !dataTicketInfo.applicant.includes("/api/users/" + decodJwt.id) &&
      !dataTicketInfo.assigned.includes("/api/users/" + decodJwt.id)
    ) {
      setError("User UnAuthorized");
      setEditorDesc("");
      // console.log(UserObserver+"----"+UserValidChangeStatus);
      return;
    }
    const dataResponse = await AddFollowUp(
      id,
      url,
      token,
      setDataFollowUp,
      setError,
      editorDesc,
      setEditorDesc,
      setLoading,
      status,
      dataTicketInfo,
      decodJwt.id
    );

    // console.log(dataResponse);
    if (dataResponse && dataResponse.ticket.id) {
      if (
        (dataTicketInfo.applicant.includes("/api/users/" + decodJwt.id) ||
          dataTicketInfo.assigned.includes("/api/users/" + decodJwt.id)) &&
        (status === "closed" ||
          (status === "in progress" &&
            dataResponse.ticket.status == "new" &&
            !dataTicketInfo.applicant.includes(
              "/api/users/" + dataResponse.users.id
            )))
      ) {
        // TODO: fetch API to update status in Ticket table
        const fetchPutTicketStatus = async () => {
          const response = await fetch(url.ticket_put + "/" + id, {
            method: "PUT",
            headers: {
              Authorization: "Bearer " + token,
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              status: status,
            }),
          });
          if (response.ok) {
            console.log("update ticket status successfully");
            const data = await response.json();
            console.log(data.status);
            dispatch(setStatusTicket({ statusTicket: data.status }));
            console.log(statusTicket);
          } else {
            console.error("error :" + response);
          }
        };

        fetchPutTicketStatus();
      }
    }

    for (const value of attachement) {
      const response = await fetch(url.Put_attachement + value.id, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + token,
        },
        body: JSON.stringify({
          followUp: "/api/follow_ups/" + dataResponse.id,
          url: value.url,
        }),
      });
      console.log("response maj attachement: " + response);
    }
    setAttachement([]);
    setStatus(statusTicket);
  };

  const handleImageUpload = () => {
    const input = document.createElement("input");
    input.setAttribute("type", "file");
    input.setAttribute("accept", "image/*");
    input.click();

    input.onchange = async () => {
      const file = input.files[0];
      const formData = new FormData();
      formData.append("file", file);

      // console.log("Clés et valeurs de FormData :");
      // console.log("Clés :", Array.from(formData.keys()));
      // console.log("Valeurs :", Array.from(formData.values()));

      try {
        // console.log('Contenu de formData :', formData);
        const response = await fetch("/api/attachements", {
          method: "POST",
          body: formData,
          headers: {
            Authorization: "Bearer " + token,
          },
        });
        console.log(response);
        if (response.ok) {
          const data = await response.json();

          const imageUrl = data.url;
          const quill = quillRef.current.getEditor();
          // const range = quill.getSelection();
          quill.insertEmbed(quill.getLength(), "image", imageUrl);
          setAttachement((prevAttachement) => [...prevAttachement, data]);
        } else {
          console.error("Erreur lors de l'upload de l'image.");
        }
      } catch (error) {
        console.error("Erreur lors de la requête d'upload :", error);
      }
    };
  };

  const modules = useMemo(
    () => ({
      toolbar: {
        container: [
          [{ header: [1, 2, false] }],
          ["bold", "italic", "underline"],
          [{ list: "ordered" }, { list: "bullet" }],
          ["image", "code-block"],
        ],
        handlers: {
          image: handleImageUpload,
        },
      },
    }),
    []
  );

  const handleEdit = async (e, id) => {
    e.preventDefault();
    const regex = /^(<p><br><\/p>|<p>\s*<\/p>)+$/;
    const currentDate = new Date()
      .toISOString()
      .replace("T", " ")
      .substr(0, 19);
    const response = await fetch(url.Put_followup + "/" + id, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + token,
      },
      body: JSON.stringify({
        content: regex.test(editorDesc) ? "" : editorDesc,
        UpdateAt: currentDate,
      }),
    });
    if (response.ok) {
      console.log("Update successfully");
      setDeleteClicked(!deleteClicked);
    } else {
      if (response.status) {
        switch (response.status) {
          case 500:
            setError("API:Error 500 Internal Server Error");
            return;
          case 404:
            setError("API:Error 404 Not Found");
            return;
          case 400:
            const error400 = await response.json();
            console.log(errorText);
            if (error400["hydra:description"]) {
              setError(error400["hydra:description"]);
            } else {
              setError(error400.detail);
            }
            return;
          case 422:
            const error422 = await response.json();
            error422.violations.forEach((element) => {
              setError(element["message"]);
            });
            return;
          default:
            break;
        }
      }
      const errorText = await response.json();
      setError(errorText.message);
      console.log(errorText);
    }
  };

  function idApplicant(item) {
    const ids = item.map((value) => value.id);
    return ids[0];
  }

  const filterOptionStatusTicket =
    statusTicket == "in progress"
      ? optionStatusTicket.filter((option) => option.status !== "new")
      : optionStatusTicket;

  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        flex: "1",
        height: "100vh",
        overflowY: "scroll",
      }}
    >
      {error !== "" && (
        <div style={{ height: "50px", margin: "0 auto" }}>
          <Alert severity="warning">{error} </Alert>
        </div>
      )}
      <div
        className="FollowUpContainer"
        style={{ paddingBottom: statusTicket == "closed" ? "100px" : "0" }}
      >
        <div className="chat-container">
          {dataTicketInfo && <div
            style={{
              width: "100%",
              margin: "0 auto 10px auto",
              borderRadius: "5px",
              backgroundColor: "lightgray",
              padding: "10px",
              color:"black"
            }}
          >
            <h2>{`Ticket_${dataTicketInfo.id}`}</h2>
            <h3 style={{textAlign:"center"}}>{`${dataTicketInfo.title}`}</h3>
            <div
              dangerouslySetInnerHTML={renderContent(
                dataTicketInfo.description
              )}
            />
          </div>}

          {loading ? (
            <ThemeProvider theme={darkTheme}>
              <CssBaseline />
              <SkeletonFollowUp />
            </ThemeProvider>
          ) : (
            dataFollowUp.map((item) => {
              const applicantId = idApplicant(item.ticket.applicant);

              return item.deleted == 0 ? (
                applicantId === item.users.id ? (
                  <div key={item.id}>
                    <ChatBubbleApplicant
                      item={item}
                      decodJwt={decodJwt}
                      setError={setError}
                      setDeleteClicked={setDeleteClicked}
                      deleteClicked={deleteClicked}
                      isOpen={isOpen}
                      setIsOpen={setIsOpen}
                      setIsEdit={setIsEdit}
                      setEditorDesc={setEditorDesc}
                      setIdFollowUp={setIdFollowUp}
                    />
                  </div>
                ) : (
                  <div key={item.id}>
                    <ChatBubbleFollowUp
                      item={item}
                      decodJwt={decodJwt}
                      setError={setError}
                      setDeleteClicked={setDeleteClicked}
                      deleteClicked={deleteClicked}
                      isOpen={isOpen}
                      setIsOpen={setIsOpen}
                      setIsEdit={setIsEdit}
                      setEditorDesc={setEditorDesc}
                      setIdFollowUp={setIdFollowUp}
                    />
                  </div>
                )
              ) : applicantId === item.users.id ? (
                <div key={item.id}>
                  <DeleteChatApplicant
                    item={item}
                    decodJwt={decodJwt}
                    setDeleteClicked={setDeleteClicked}
                    deleteClicked={deleteClicked}
                  />
                </div>
              ) : (
                <div key={item.id}>
                  <DeleteChatFollowUp
                    item={item}
                    decodJwt={decodJwt}
                    setDeleteClicked={setDeleteClicked}
                    deleteClicked={deleteClicked}
                  />
                </div>
              );
            })
          )}
        </div>
        {!loading && statusTicket !== "closed" && (
          <div className={`write ${isOpen ? "writeOpen" : ""}`}>
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
              }}
            >
              {!isEdit && (
                <ThemeProvider theme={darkTheme}>
                  <CssBaseline />
                  <FormControl
                    sx={{ m: 1, minWidth: 120 }}
                    size="small"
                    color="secondary"
                  >
                    <InputLabel id="demo-simple-select-label">
                      Status
                    </InputLabel>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      value={status}
                      label="Status"
                      onChange={(e) => setStatus(e.target.value)}
                    >
                      {filterOptionStatusTicket.map((opt) => (
                        <MenuItem key={opt.id} value={opt.status}>
                          <BadgeCircle size={"small"} bg={opt.color} />
                          {opt.label}
                        </MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                </ThemeProvider>
              )}
              {isOpen ? (
                <ExpandLessOutlinedIcon
                  fontSize="large"
                  style={{
                    transition: "transform 0.3s ease-in-out",
                    margin: "auto",
                  }}
                  onClick={toggleDropdown}
                />
              ) : (
                <ExpandLessOutlinedIcon
                  fontSize="large"
                  style={{
                    transition: "transform 0.3s ease-in-out",
                    transform: "rotate(180deg)",
                    margin: "auto",
                  }}
                  onClick={toggleDropdown}
                />
              )}
              {!isEdit ? (
                <div style={{ display: "flex" }}>
                  <Button
                    style={{ margin: "5px 10px" }}
                    variant="contained"
                    color="secondary"
                    endIcon={<Send />}
                    onClick={handleAddFollowUp}
                  >
                    ADD
                  </Button>
                </div>
              ) : (
                <>
                  <HighlightOff
                    onClick={() => {
                      setIsEdit(false);
                      setEditorDesc("");
                    }}
                    sx={{
                      margin: "0 5px",
                      cursor: "pointer",
                    }}
                    className="cancelEdit"
                  />
                  <Button
                    style={{ margin: "5px 10px" }}
                    variant="contained"
                    color="secondary"
                    endIcon={<Send />}
                    onClick={(e) => handleEdit(e, idFollowUp)}
                  >
                    EDIT
                  </Button>
                </>
              )}
            </div>
            <div className="content">
              <ReactQuill
                ref={quillRef}
                theme="snow"
                className="textareaField"
                value={editorDesc}
                onChange={(value) => setEditorDesc(value, editorDesc)}
                modules={modules}
              />
            </div>
          </div>
        )}
      </div>
    </div>
  );
}

export default FollowUp;
