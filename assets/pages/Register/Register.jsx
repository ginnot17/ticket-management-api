import React, { useEffect, useState } from "react";
import "./Register.scss";
import {
  Button,
  FormControl,
  FormHelperText,
  IconButton,
  InputAdornment,
  InputLabel,
  MenuItem,
  Select,
  Skeleton,
  TextField,
  Typography,
} from "@mui/material";
import { Send } from "@mui/icons-material";
import { useDispatch, useSelector } from "react-redux";
import { url } from "../../urlApi";
import { Link, Navigate, useNavigate } from "react-router-dom";
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import { ThemeProvider, createTheme } from "@mui/material/styles";
import CssBaseline from "@mui/material/CssBaseline";

function Login() {
  const Theme = useSelector((state) => state.theme.value);

  const darkTheme = createTheme({
    palette: {
      mode: Theme,
    },
  });
  const [error_field, setError] = useState({
    lastName: "",
    firstName: "",
    password: "",
    email: "",
    groups: "",
    passwordConfirm: "",
  });
  const [email, setEmail] = useState("");
  const [FirstName, setFirstName] = useState("");
  const [LastName, setLastName] = useState("");
  const [password, setPassword] = useState("");
  const [group, setGroup] = useState("");
  const [ConfirmPassword, setConfirmPassword] = useState("");
  const [showPassword, setShowPassword] = useState(false);
  const [showPasswordConfirm, setShowPasswordConfirm] = useState(false);
  const [dataGroup, setDataGroup] = useState([]);
  const [loading, setLoading] = useState(true);

  const handleClickShowPassword = (type) =>
    type === "Pwd"
      ? setShowPassword((show) => !show)
      : setShowPasswordConfirm((sh) => !sh);

  const handleMouseDownPassword = (e) => {
    e.preventDefault();
  };
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const handleRegister = async (e) => {
    e.preventDefault();
    setLoading(true)
    const ErrorPersist = {};
    if (password !== ConfirmPassword) {
      ErrorPersist.passwordConfirm =
        "Password and Confirm password is not compliant";
      setError(ErrorPersist);
      setLoading(false)
      return;
    }
    try {
      const response = await fetch(url.register, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          email: email,
          password: password,
          lastName: LastName,
          firstName: FirstName,
          groups: group,
        }),
      });

      const data = await response.json();
      console.log(data);
      if (data.violations) {
        data.violations.forEach((element) => {
          ErrorPersist[element.propertyPath] = element.message;
        });
        setError(ErrorPersist);
        // console.log(data.violations);
      } else {
        console.log(data);
        setError({});
        navigate("/");
      }
      setLoading(false)
    } catch (error) {
      console.log("Error in:" + error);
      setLoading(false)
    }
  };

  useEffect(() => {
    fetchListGroup();
  }, []);

  const fetchListGroup = async () => {
    
    try {
      const responseData = await fetch(url.group, {
        method: "GET",
        headers: {
          "content-type": "application/json",
          Accept: "application/json",
        },
      });
      if (responseData.ok) {
        const jsonData = await responseData.json();

        setDataGroup(jsonData);
        if (jsonData.length > 0) {
          setGroup(`/api/groups/${jsonData[0].id}`);
        }
      } else {
        console.error("Erreur lors de la réponse API :", responseData.status);
      }
      setLoading(false);
    } catch (error) {
      console.log(error);
    }
  };

  console.log(dataGroup)

  return (
    <div className="RegisterContainer">
      <div className="formCountainer">
        <h1>Sign Up</h1>
        <form action="">
          <ThemeProvider theme={darkTheme}>
            <CssBaseline />
            {loading ? (
              <Skeleton
                sx={{ height: "3rem" }}
                animation="wave"
                variant="rectangular"
              />
            ) : (
              <TextField
                error={error_field.email ? true : false}
                helperText={(error_field.email && `${error_field.email}`) || ""}
                color="secondary"
                onChange={(e) => setEmail(e.target.value)}
                label="Email"
                variant="outlined"
                value={email}
              />
            )}
            {loading ? (
              <Skeleton
                sx={{ height: "3rem" }}
                animation="wave"
                variant="rectangular"
              />
            ) : (
              <TextField
                error={error_field.firstName ? true : false}
                helperText={
                  (error_field.firstName && `${error_field.firstName}`) || ""
                }
                color="secondary"
                onChange={(e) => setFirstName(e.target.value)}
                label="First name"
                variant="outlined"
                value={FirstName}
              />
            )}
            {loading ? (
              <Skeleton
                sx={{ height: "3rem" }}
                animation="wave"
                variant="rectangular"
              />
            ) : (
              <TextField
                error={error_field.lastName ? true : false}
                helperText={
                  (error_field.lastName && `${error_field.lastName}`) || ""
                }
                color="secondary"
                onChange={(e) => setLastName(e.target.value)}
                label="Last name"
                variant="outlined"
                value={LastName}
              />
            )}
            {loading ? (
              <Skeleton
                sx={{ height: "3rem" }}
                animation="wave"
                variant="rectangular"
              />
            ) : (
              <FormControl error={error_field.groups ? true : false}>
                <InputLabel color="secondary" id="demo-simple-select-label">
                  Group
                </InputLabel>
                <Select
                  color="secondary"
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={group}
                  label="Group"
                  onChange={(e) => setGroup(e.target.value)}
                >
                  { dataGroup &&
                    dataGroup.map((value) => (
                      <MenuItem
                        value={`/api/groups/${value.id}`}
                        key={`${value.id}`}
                      >{`${value.name}`}</MenuItem>
                    ))}
                </Select>
                {error_field.groups && (
                  <FormHelperText>{error_field.groups}</FormHelperText>
                )}
              </FormControl>
            )}
            {loading ? (
              <Skeleton
                sx={{ height: "3rem" }}
                animation="wave"
                variant="rectangular"
              />
            ) : (
              <TextField
                error={error_field.password ? true : false}
                helperText={
                  (error_field.password && `${error_field.password}`) || ""
                }
                color="secondary"
                onChange={(e) => setPassword(e.target.value)}
                label="Password"
                value={password}
                type={showPassword ? "text" : "password"}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        onClick={() => handleClickShowPassword("Pwd")}
                        onMouseDown={handleMouseDownPassword}
                        edge="end"
                      >
                        {showPassword ? <VisibilityOff /> : <Visibility />}
                      </IconButton>
                    </InputAdornment>
                  ),
                }}
              />
            )}
            {loading ? (
              <Skeleton
                sx={{ height: "3rem" }}
                animation="wave"
                variant="rectangular"
              />
            ) : (
              <TextField
                error={error_field.passwordConfirm ? true : false}
                helperText={
                  (error_field.passwordConfirm &&
                    `${error_field.passwordConfirm}`) ||
                  ""
                }
                color="secondary"
                onChange={(e) => setConfirmPassword(e.target.value)}
                label="Confirm Password"
                variant="outlined"
                value={ConfirmPassword}
                type={showPasswordConfirm ? "text" : "password"}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        onClick={() => handleClickShowPassword("ConfPwd")}
                        onMouseDown={handleMouseDownPassword}
                        edge="end"
                      >
                        {showPasswordConfirm ? (
                          <VisibilityOff />
                        ) : (
                          <Visibility />
                        )}
                      </IconButton>
                    </InputAdornment>
                  ),
                }}
              />
            )}
          </ThemeProvider>
          {loading ? (
            <ThemeProvider theme={darkTheme}>
              <CssBaseline />
              <Skeleton
                sx={{ height: "3rem" }}
                animation="wave"
                variant="rectangular"
              />
            </ThemeProvider>
          ) : (
            <Button
              onClick={handleRegister}
              variant="contained"
              color="secondary"
              endIcon={<Send />}
            >
              Register
            </Button>
          )}

          {loading ? (
            <ThemeProvider theme={darkTheme}>
              <CssBaseline />
              <Typography
                sx={{ width: "70%", margin: "auto" }}
                variant="caption"
              >
                <Skeleton />{" "}
              </Typography>
            </ThemeProvider>
          ) : (
            <p>
              Have you an account{" "}
              <Link className="signIn" to="/login">
                Sign In
              </Link>
            </p>
          )}
        </form>
      </div>
    </div>
  );
}

export default Login;
