--
-- PostgreSQL database dump
--

-- Dumped from database version 15.4
-- Dumped by pg_dump version 15.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: notify_messenger_messages(); Type: FUNCTION; Schema: public; Owner: app
--

CREATE FUNCTION public.notify_messenger_messages() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
            BEGIN
                PERFORM pg_notify('messenger_messages', NEW.queue_name::text);
                RETURN NEW;
            END;
        $$;


ALTER FUNCTION public.notify_messenger_messages() OWNER TO app;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: attachement; Type: TABLE; Schema: public; Owner: app
--

CREATE TABLE public.attachement (
    id integer NOT NULL,
    url text,
    follow_up_id integer
);


ALTER TABLE public.attachement OWNER TO app;

--
-- Name: attachement_id_seq; Type: SEQUENCE; Schema: public; Owner: app
--

CREATE SEQUENCE public.attachement_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.attachement_id_seq OWNER TO app;

--
-- Name: doctrine_migration_versions; Type: TABLE; Schema: public; Owner: app
--

CREATE TABLE public.doctrine_migration_versions (
    version character varying(191) NOT NULL,
    executed_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
    execution_time integer
);


ALTER TABLE public.doctrine_migration_versions OWNER TO app;

--
-- Name: follow_up; Type: TABLE; Schema: public; Owner: app
--

CREATE TABLE public.follow_up (
    id integer NOT NULL,
    users_id integer,
    ticket_id integer,
    content text NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    update_at timestamp without time zone
);


ALTER TABLE public.follow_up OWNER TO app;

--
-- Name: COLUMN follow_up.created_at; Type: COMMENT; Schema: public; Owner: app
--

COMMENT ON COLUMN public.follow_up.created_at IS '(DC2Type:datetime_immutable)';


--
-- Name: COLUMN follow_up.update_at; Type: COMMENT; Schema: public; Owner: app
--

COMMENT ON COLUMN public.follow_up.update_at IS '(DC2Type:datetime_immutable)';


--
-- Name: follow_up_id_seq; Type: SEQUENCE; Schema: public; Owner: app
--

CREATE SEQUENCE public.follow_up_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.follow_up_id_seq OWNER TO app;

--
-- Name: group; Type: TABLE; Schema: public; Owner: app
--

CREATE TABLE public."group" (
    id integer NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public."group" OWNER TO app;

--
-- Name: group_id_seq; Type: SEQUENCE; Schema: public; Owner: app
--

CREATE SEQUENCE public.group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.group_id_seq OWNER TO app;

--
-- Name: group_ticket_applicant; Type: TABLE; Schema: public; Owner: app
--

CREATE TABLE public.group_ticket_applicant (
    group_id integer NOT NULL,
    ticket_id integer NOT NULL
);


ALTER TABLE public.group_ticket_applicant OWNER TO app;

--
-- Name: group_ticket_assigned; Type: TABLE; Schema: public; Owner: app
--

CREATE TABLE public.group_ticket_assigned (
    group_id integer NOT NULL,
    ticket_id integer NOT NULL
);


ALTER TABLE public.group_ticket_assigned OWNER TO app;

--
-- Name: group_ticket_observer; Type: TABLE; Schema: public; Owner: app
--

CREATE TABLE public.group_ticket_observer (
    group_id integer NOT NULL,
    ticket_id integer NOT NULL
);


ALTER TABLE public.group_ticket_observer OWNER TO app;

--
-- Name: messenger_messages; Type: TABLE; Schema: public; Owner: app
--

CREATE TABLE public.messenger_messages (
    id bigint NOT NULL,
    body text NOT NULL,
    headers text NOT NULL,
    queue_name character varying(190) NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    available_at timestamp(0) without time zone NOT NULL,
    delivered_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone
);


ALTER TABLE public.messenger_messages OWNER TO app;

--
-- Name: COLUMN messenger_messages.created_at; Type: COMMENT; Schema: public; Owner: app
--

COMMENT ON COLUMN public.messenger_messages.created_at IS '(DC2Type:datetime_immutable)';


--
-- Name: COLUMN messenger_messages.available_at; Type: COMMENT; Schema: public; Owner: app
--

COMMENT ON COLUMN public.messenger_messages.available_at IS '(DC2Type:datetime_immutable)';


--
-- Name: COLUMN messenger_messages.delivered_at; Type: COMMENT; Schema: public; Owner: app
--

COMMENT ON COLUMN public.messenger_messages.delivered_at IS '(DC2Type:datetime_immutable)';


--
-- Name: messenger_messages_id_seq; Type: SEQUENCE; Schema: public; Owner: app
--

CREATE SEQUENCE public.messenger_messages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.messenger_messages_id_seq OWNER TO app;

--
-- Name: messenger_messages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: app
--

ALTER SEQUENCE public.messenger_messages_id_seq OWNED BY public.messenger_messages.id;


--
-- Name: ticket; Type: TABLE; Schema: public; Owner: app
--

CREATE TABLE public.ticket (
    id integer NOT NULL,
    title character varying(255) NOT NULL,
    description text NOT NULL,
    created_at timestamp(0) without time zone NOT NULL
);


ALTER TABLE public.ticket OWNER TO app;

--
-- Name: COLUMN ticket.created_at; Type: COMMENT; Schema: public; Owner: app
--

COMMENT ON COLUMN public.ticket.created_at IS '(DC2Type:datetime_immutable)';


--
-- Name: ticket_applicant; Type: TABLE; Schema: public; Owner: app
--

CREATE TABLE public.ticket_applicant (
    ticket_id integer NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.ticket_applicant OWNER TO app;

--
-- Name: ticket_assigned; Type: TABLE; Schema: public; Owner: app
--

CREATE TABLE public.ticket_assigned (
    ticket_id integer NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.ticket_assigned OWNER TO app;

--
-- Name: ticket_id_seq; Type: SEQUENCE; Schema: public; Owner: app
--

CREATE SEQUENCE public.ticket_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ticket_id_seq OWNER TO app;

--
-- Name: ticket_observer; Type: TABLE; Schema: public; Owner: app
--

CREATE TABLE public.ticket_observer (
    ticket_id integer NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.ticket_observer OWNER TO app;

--
-- Name: user; Type: TABLE; Schema: public; Owner: app
--

CREATE TABLE public."user" (
    id integer NOT NULL,
    email character varying(180) NOT NULL,
    roles json NOT NULL,
    password character varying(255) NOT NULL,
    groups_id integer
);


ALTER TABLE public."user" OWNER TO app;

--
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: app
--

CREATE SEQUENCE public.user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_id_seq OWNER TO app;

--
-- Name: messenger_messages id; Type: DEFAULT; Schema: public; Owner: app
--

ALTER TABLE ONLY public.messenger_messages ALTER COLUMN id SET DEFAULT nextval('public.messenger_messages_id_seq'::regclass);


--
-- Name: attachement attachement_pkey; Type: CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY public.attachement
    ADD CONSTRAINT attachement_pkey PRIMARY KEY (id);


--
-- Name: doctrine_migration_versions doctrine_migration_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY public.doctrine_migration_versions
    ADD CONSTRAINT doctrine_migration_versions_pkey PRIMARY KEY (version);


--
-- Name: follow_up follow_up_pkey; Type: CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY public.follow_up
    ADD CONSTRAINT follow_up_pkey PRIMARY KEY (id);


--
-- Name: group group_pkey; Type: CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY public."group"
    ADD CONSTRAINT group_pkey PRIMARY KEY (id);


--
-- Name: group_ticket_applicant group_ticket_applicant_pkey; Type: CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY public.group_ticket_applicant
    ADD CONSTRAINT group_ticket_applicant_pkey PRIMARY KEY (group_id, ticket_id);


--
-- Name: group_ticket_assigned group_ticket_assigned_pkey; Type: CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY public.group_ticket_assigned
    ADD CONSTRAINT group_ticket_assigned_pkey PRIMARY KEY (group_id, ticket_id);


--
-- Name: group_ticket_observer group_ticket_observer_pkey; Type: CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY public.group_ticket_observer
    ADD CONSTRAINT group_ticket_observer_pkey PRIMARY KEY (group_id, ticket_id);


--
-- Name: messenger_messages messenger_messages_pkey; Type: CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY public.messenger_messages
    ADD CONSTRAINT messenger_messages_pkey PRIMARY KEY (id);


--
-- Name: ticket_applicant ticket_applicant_pkey; Type: CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY public.ticket_applicant
    ADD CONSTRAINT ticket_applicant_pkey PRIMARY KEY (ticket_id, user_id);


--
-- Name: ticket_assigned ticket_assigned_pkey; Type: CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY public.ticket_assigned
    ADD CONSTRAINT ticket_assigned_pkey PRIMARY KEY (ticket_id, user_id);


--
-- Name: ticket_observer ticket_observer_pkey; Type: CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY public.ticket_observer
    ADD CONSTRAINT ticket_observer_pkey PRIMARY KEY (ticket_id, user_id);


--
-- Name: ticket ticket_pkey; Type: CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY public.ticket
    ADD CONSTRAINT ticket_pkey PRIMARY KEY (id);


--
-- Name: user user_pkey; Type: CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- Name: idx_1e06b866700047d2; Type: INDEX; Schema: public; Owner: app
--

CREATE INDEX idx_1e06b866700047d2 ON public.ticket_observer USING btree (ticket_id);


--
-- Name: idx_1e06b866a76ed395; Type: INDEX; Schema: public; Owner: app
--

CREATE INDEX idx_1e06b866a76ed395 ON public.ticket_observer USING btree (user_id);


--
-- Name: idx_3060adc2700047d2; Type: INDEX; Schema: public; Owner: app
--

CREATE INDEX idx_3060adc2700047d2 ON public.group_ticket_assigned USING btree (ticket_id);


--
-- Name: idx_3060adc2fe54d947; Type: INDEX; Schema: public; Owner: app
--

CREATE INDEX idx_3060adc2fe54d947 ON public.group_ticket_assigned USING btree (group_id);


--
-- Name: idx_5097ca53700047d2; Type: INDEX; Schema: public; Owner: app
--

CREATE INDEX idx_5097ca53700047d2 ON public.ticket_applicant USING btree (ticket_id);


--
-- Name: idx_5097ca53a76ed395; Type: INDEX; Schema: public; Owner: app
--

CREATE INDEX idx_5097ca53a76ed395 ON public.ticket_applicant USING btree (user_id);


--
-- Name: idx_75ea56e016ba31db; Type: INDEX; Schema: public; Owner: app
--

CREATE INDEX idx_75ea56e016ba31db ON public.messenger_messages USING btree (delivered_at);


--
-- Name: idx_75ea56e0e3bd61ce; Type: INDEX; Schema: public; Owner: app
--

CREATE INDEX idx_75ea56e0e3bd61ce ON public.messenger_messages USING btree (available_at);


--
-- Name: idx_75ea56e0fb7336f0; Type: INDEX; Schema: public; Owner: app
--

CREATE INDEX idx_75ea56e0fb7336f0 ON public.messenger_messages USING btree (queue_name);


--
-- Name: idx_7bbc5a9c67b3b43d; Type: INDEX; Schema: public; Owner: app
--

CREATE INDEX idx_7bbc5a9c67b3b43d ON public.follow_up USING btree (users_id);


--
-- Name: idx_7bbc5a9c700047d2; Type: INDEX; Schema: public; Owner: app
--

CREATE INDEX idx_7bbc5a9c700047d2 ON public.follow_up USING btree (ticket_id);


--
-- Name: idx_8d93d649f373dcf; Type: INDEX; Schema: public; Owner: app
--

CREATE INDEX idx_8d93d649f373dcf ON public."user" USING btree (groups_id);


--
-- Name: idx_8ef3301b700047d2; Type: INDEX; Schema: public; Owner: app
--

CREATE INDEX idx_8ef3301b700047d2 ON public.ticket_assigned USING btree (ticket_id);


--
-- Name: idx_8ef3301ba76ed395; Type: INDEX; Schema: public; Owner: app
--

CREATE INDEX idx_8ef3301ba76ed395 ON public.ticket_assigned USING btree (user_id);


--
-- Name: idx_901c196110cbcea6; Type: INDEX; Schema: public; Owner: app
--

CREATE INDEX idx_901c196110cbcea6 ON public.attachement USING btree (follow_up_id);


--
-- Name: idx_a09525bf700047d2; Type: INDEX; Schema: public; Owner: app
--

CREATE INDEX idx_a09525bf700047d2 ON public.group_ticket_observer USING btree (ticket_id);


--
-- Name: idx_a09525bffe54d947; Type: INDEX; Schema: public; Owner: app
--

CREATE INDEX idx_a09525bffe54d947 ON public.group_ticket_observer USING btree (group_id);


--
-- Name: idx_af2633be700047d2; Type: INDEX; Schema: public; Owner: app
--

CREATE INDEX idx_af2633be700047d2 ON public.group_ticket_applicant USING btree (ticket_id);


--
-- Name: idx_af2633befe54d947; Type: INDEX; Schema: public; Owner: app
--

CREATE INDEX idx_af2633befe54d947 ON public.group_ticket_applicant USING btree (group_id);


--
-- Name: uniq_8d93d649e7927c74; Type: INDEX; Schema: public; Owner: app
--

CREATE UNIQUE INDEX uniq_8d93d649e7927c74 ON public."user" USING btree (email);


--
-- Name: messenger_messages notify_trigger; Type: TRIGGER; Schema: public; Owner: app
--

CREATE TRIGGER notify_trigger AFTER INSERT OR UPDATE ON public.messenger_messages FOR EACH ROW EXECUTE FUNCTION public.notify_messenger_messages();


--
-- Name: ticket_observer fk_1e06b866700047d2; Type: FK CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY public.ticket_observer
    ADD CONSTRAINT fk_1e06b866700047d2 FOREIGN KEY (ticket_id) REFERENCES public.ticket(id) ON DELETE CASCADE;


--
-- Name: ticket_observer fk_1e06b866a76ed395; Type: FK CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY public.ticket_observer
    ADD CONSTRAINT fk_1e06b866a76ed395 FOREIGN KEY (user_id) REFERENCES public."user"(id) ON DELETE CASCADE;


--
-- Name: group_ticket_assigned fk_3060adc2700047d2; Type: FK CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY public.group_ticket_assigned
    ADD CONSTRAINT fk_3060adc2700047d2 FOREIGN KEY (ticket_id) REFERENCES public.ticket(id) ON DELETE CASCADE;


--
-- Name: group_ticket_assigned fk_3060adc2fe54d947; Type: FK CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY public.group_ticket_assigned
    ADD CONSTRAINT fk_3060adc2fe54d947 FOREIGN KEY (group_id) REFERENCES public."group"(id) ON DELETE CASCADE;


--
-- Name: ticket_applicant fk_5097ca53700047d2; Type: FK CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY public.ticket_applicant
    ADD CONSTRAINT fk_5097ca53700047d2 FOREIGN KEY (ticket_id) REFERENCES public.ticket(id) ON DELETE CASCADE;


--
-- Name: ticket_applicant fk_5097ca53a76ed395; Type: FK CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY public.ticket_applicant
    ADD CONSTRAINT fk_5097ca53a76ed395 FOREIGN KEY (user_id) REFERENCES public."user"(id) ON DELETE CASCADE;


--
-- Name: follow_up fk_7bbc5a9c67b3b43d; Type: FK CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY public.follow_up
    ADD CONSTRAINT fk_7bbc5a9c67b3b43d FOREIGN KEY (users_id) REFERENCES public."user"(id);


--
-- Name: follow_up fk_7bbc5a9c700047d2; Type: FK CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY public.follow_up
    ADD CONSTRAINT fk_7bbc5a9c700047d2 FOREIGN KEY (ticket_id) REFERENCES public.ticket(id);


--
-- Name: user fk_8d93d649f373dcf; Type: FK CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT fk_8d93d649f373dcf FOREIGN KEY (groups_id) REFERENCES public."group"(id);


--
-- Name: ticket_assigned fk_8ef3301b700047d2; Type: FK CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY public.ticket_assigned
    ADD CONSTRAINT fk_8ef3301b700047d2 FOREIGN KEY (ticket_id) REFERENCES public.ticket(id) ON DELETE CASCADE;


--
-- Name: ticket_assigned fk_8ef3301ba76ed395; Type: FK CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY public.ticket_assigned
    ADD CONSTRAINT fk_8ef3301ba76ed395 FOREIGN KEY (user_id) REFERENCES public."user"(id) ON DELETE CASCADE;


--
-- Name: attachement fk_901c196110cbcea6; Type: FK CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY public.attachement
    ADD CONSTRAINT fk_901c196110cbcea6 FOREIGN KEY (follow_up_id) REFERENCES public.follow_up(id);


--
-- Name: group_ticket_observer fk_a09525bf700047d2; Type: FK CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY public.group_ticket_observer
    ADD CONSTRAINT fk_a09525bf700047d2 FOREIGN KEY (ticket_id) REFERENCES public.ticket(id) ON DELETE CASCADE;


--
-- Name: group_ticket_observer fk_a09525bffe54d947; Type: FK CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY public.group_ticket_observer
    ADD CONSTRAINT fk_a09525bffe54d947 FOREIGN KEY (group_id) REFERENCES public."group"(id) ON DELETE CASCADE;


--
-- Name: group_ticket_applicant fk_af2633be700047d2; Type: FK CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY public.group_ticket_applicant
    ADD CONSTRAINT fk_af2633be700047d2 FOREIGN KEY (ticket_id) REFERENCES public.ticket(id) ON DELETE CASCADE;


--
-- Name: group_ticket_applicant fk_af2633befe54d947; Type: FK CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY public.group_ticket_applicant
    ADD CONSTRAINT fk_af2633befe54d947 FOREIGN KEY (group_id) REFERENCES public."group"(id) ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

