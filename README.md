# Composer
composer install
# Utilisant npm
npm install

# Ou utilisant yarn
yarn install

# Générer la clé d'application Symfony
php bin/console secrets:generate-keys

# Base de donnée

php bin/console doctrine:database:create

php bin/console doctrine:migrations:migrate

# Necessaire pour initialiser la liste groupes
INSERT INTO "group" ("id", "name") VALUES (1, 'IT');
INSERT INTO "group" ("id", "name") VALUES (2, 'DEV');
INSERT INTO "group" ("id", "name") VALUES (3, 'SYS');

# Lancement app
npm run dev

make start