<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230906171814 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE follow_up_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE follow_up (id INT NOT NULL, users_id INT DEFAULT NULL, ticket_id INT DEFAULT NULL, content TEXT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, update_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_7BBC5A9C67B3B43D ON follow_up (users_id)');
        $this->addSql('CREATE INDEX IDX_7BBC5A9C700047D2 ON follow_up (ticket_id)');
        $this->addSql('COMMENT ON COLUMN follow_up.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN follow_up.update_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE follow_up ADD CONSTRAINT FK_7BBC5A9C67B3B43D FOREIGN KEY (users_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE follow_up ADD CONSTRAINT FK_7BBC5A9C700047D2 FOREIGN KEY (ticket_id) REFERENCES ticket (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE follow_up_id_seq CASCADE');
        $this->addSql('ALTER TABLE follow_up DROP CONSTRAINT FK_7BBC5A9C67B3B43D');
        $this->addSql('ALTER TABLE follow_up DROP CONSTRAINT FK_7BBC5A9C700047D2');
        $this->addSql('DROP TABLE follow_up');
    }
}
