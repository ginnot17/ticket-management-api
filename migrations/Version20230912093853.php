<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230912093853 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        // $this->addSql('ALTER TABLE follow_up ALTER update_at SET NOT NULL');
        $this->addSql('ALTER TABLE "group" DROP CONSTRAINT fk_6dc044c567b3b43d');
        $this->addSql('DROP INDEX idx_6dc044c567b3b43d');
        $this->addSql('ALTER TABLE "group" DROP users_id');
        $this->addSql('ALTER TABLE "user" ADD groups_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE "user" ADD CONSTRAINT FK_8D93D649F373DCF FOREIGN KEY (groups_id) REFERENCES "group" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_8D93D649F373DCF ON "user" (groups_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE "user" DROP CONSTRAINT FK_8D93D649F373DCF');
        $this->addSql('DROP INDEX IDX_8D93D649F373DCF');
        $this->addSql('ALTER TABLE "user" DROP groups_id');
        $this->addSql('ALTER TABLE "group" ADD users_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE "group" ADD CONSTRAINT fk_6dc044c567b3b43d FOREIGN KEY (users_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_6dc044c567b3b43d ON "group" (users_id)');
        // $this->addSql('ALTER TABLE follow_up ALTER update_at DROP NOT NULL');
    }
}
