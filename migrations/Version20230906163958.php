<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230906163958 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE ticket_observer (ticket_id INT NOT NULL, user_id INT NOT NULL, PRIMARY KEY(ticket_id, user_id))');
        $this->addSql('CREATE INDEX IDX_1E06B866700047D2 ON ticket_observer (ticket_id)');
        $this->addSql('CREATE INDEX IDX_1E06B866A76ED395 ON ticket_observer (user_id)');
        $this->addSql('CREATE TABLE ticket_applicant (ticket_id INT NOT NULL, user_id INT NOT NULL, PRIMARY KEY(ticket_id, user_id))');
        $this->addSql('CREATE INDEX IDX_5097CA53700047D2 ON ticket_applicant (ticket_id)');
        $this->addSql('CREATE INDEX IDX_5097CA53A76ED395 ON ticket_applicant (user_id)');
        $this->addSql('CREATE TABLE ticket_assigned (ticket_id INT NOT NULL, user_id INT NOT NULL, PRIMARY KEY(ticket_id, user_id))');
        $this->addSql('CREATE INDEX IDX_8EF3301B700047D2 ON ticket_assigned (ticket_id)');
        $this->addSql('CREATE INDEX IDX_8EF3301BA76ED395 ON ticket_assigned (user_id)');
        $this->addSql('ALTER TABLE ticket_observer ADD CONSTRAINT FK_1E06B866700047D2 FOREIGN KEY (ticket_id) REFERENCES ticket (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE ticket_observer ADD CONSTRAINT FK_1E06B866A76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE ticket_applicant ADD CONSTRAINT FK_5097CA53700047D2 FOREIGN KEY (ticket_id) REFERENCES ticket (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE ticket_applicant ADD CONSTRAINT FK_5097CA53A76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE ticket_assigned ADD CONSTRAINT FK_8EF3301B700047D2 FOREIGN KEY (ticket_id) REFERENCES ticket (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE ticket_assigned ADD CONSTRAINT FK_8EF3301BA76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE ticket_observer DROP CONSTRAINT FK_1E06B866700047D2');
        $this->addSql('ALTER TABLE ticket_observer DROP CONSTRAINT FK_1E06B866A76ED395');
        $this->addSql('ALTER TABLE ticket_applicant DROP CONSTRAINT FK_5097CA53700047D2');
        $this->addSql('ALTER TABLE ticket_applicant DROP CONSTRAINT FK_5097CA53A76ED395');
        $this->addSql('ALTER TABLE ticket_assigned DROP CONSTRAINT FK_8EF3301B700047D2');
        $this->addSql('ALTER TABLE ticket_assigned DROP CONSTRAINT FK_8EF3301BA76ED395');
        $this->addSql('DROP TABLE ticket_observer');
        $this->addSql('DROP TABLE ticket_applicant');
        $this->addSql('DROP TABLE ticket_assigned');
    }
}
