<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230912091333 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE "group" (id INT NOT NULL, users_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_6DC044C567B3B43D ON "group" (users_id)');
        $this->addSql('CREATE TABLE group_ticket_assigned (group_id INT NOT NULL, ticket_id INT NOT NULL, PRIMARY KEY(group_id, ticket_id))');
        $this->addSql('CREATE INDEX IDX_3060ADC2FE54D947 ON group_ticket_assigned (group_id)');
        $this->addSql('CREATE INDEX IDX_3060ADC2700047D2 ON group_ticket_assigned (ticket_id)');
        $this->addSql('CREATE TABLE group_ticket_observer (group_id INT NOT NULL, ticket_id INT NOT NULL, PRIMARY KEY(group_id, ticket_id))');
        $this->addSql('CREATE INDEX IDX_A09525BFFE54D947 ON group_ticket_observer (group_id)');
        $this->addSql('CREATE INDEX IDX_A09525BF700047D2 ON group_ticket_observer (ticket_id)');
        $this->addSql('CREATE TABLE group_ticket_applicant (group_id INT NOT NULL, ticket_id INT NOT NULL, PRIMARY KEY(group_id, ticket_id))');
        $this->addSql('CREATE INDEX IDX_AF2633BEFE54D947 ON group_ticket_applicant (group_id)');
        $this->addSql('CREATE INDEX IDX_AF2633BE700047D2 ON group_ticket_applicant (ticket_id)');
        $this->addSql('ALTER TABLE "group" ADD CONSTRAINT FK_6DC044C567B3B43D FOREIGN KEY (users_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE group_ticket_assigned ADD CONSTRAINT FK_3060ADC2FE54D947 FOREIGN KEY (group_id) REFERENCES "group" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE group_ticket_assigned ADD CONSTRAINT FK_3060ADC2700047D2 FOREIGN KEY (ticket_id) REFERENCES ticket (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE group_ticket_observer ADD CONSTRAINT FK_A09525BFFE54D947 FOREIGN KEY (group_id) REFERENCES "group" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE group_ticket_observer ADD CONSTRAINT FK_A09525BF700047D2 FOREIGN KEY (ticket_id) REFERENCES ticket (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE group_ticket_applicant ADD CONSTRAINT FK_AF2633BEFE54D947 FOREIGN KEY (group_id) REFERENCES "group" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE group_ticket_applicant ADD CONSTRAINT FK_AF2633BE700047D2 FOREIGN KEY (ticket_id) REFERENCES ticket (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        // $this->addSql('ALTER TABLE follow_up ALTER update_at SET NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE "group" DROP CONSTRAINT FK_6DC044C567B3B43D');
        $this->addSql('ALTER TABLE group_ticket_assigned DROP CONSTRAINT FK_3060ADC2FE54D947');
        $this->addSql('ALTER TABLE group_ticket_assigned DROP CONSTRAINT FK_3060ADC2700047D2');
        $this->addSql('ALTER TABLE group_ticket_observer DROP CONSTRAINT FK_A09525BFFE54D947');
        $this->addSql('ALTER TABLE group_ticket_observer DROP CONSTRAINT FK_A09525BF700047D2');
        $this->addSql('ALTER TABLE group_ticket_applicant DROP CONSTRAINT FK_AF2633BEFE54D947');
        $this->addSql('ALTER TABLE group_ticket_applicant DROP CONSTRAINT FK_AF2633BE700047D2');
        $this->addSql('DROP TABLE "group"');
        $this->addSql('DROP TABLE group_ticket_assigned');
        $this->addSql('DROP TABLE group_ticket_observer');
        $this->addSql('DROP TABLE group_ticket_applicant');
        // $this->addSql('ALTER TABLE follow_up ALTER update_at DROP NOT NULL');
    }
}
