<?php

namespace App\Controller;

use App\Entity\FollowUp;
use App\Entity\Ticket;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class FollowUpControllers extends AbstractController
{
    public function __construct(
        private EntityManagerInterface $manager,
        private Security               $security
    )
    {
    }

    /**
     *
     * @return array
     */
    public function __invoke(Request $request)
    {

        if ($request->getMethod() == 'GET') {
            $data = $this->getFollowUpByTicket($request);
        } elseif ($request->getMethod() == 'POST') {
            $data = $this->postFollowUpByTicket($request);
        }

        return $data;
    }

    private function getFollowUpByTicket(Request $request)
    {
        $groupTicket = $this->manager->getRepository(Ticket::class)->findOneBy(['id' => $request->query->get('id')]);
        $this->verifGroupUserToTicketGroup($groupTicket);
        $data = $this->manager->getRepository(FollowUp::class)->findBy(['ticket' => $request->query->get('id')], ['id' => 'ASC']);

        return $data;
    }

    private function postFollowUpByTicket(Request $request)
    {
        $groupTicket = $this->manager->getRepository(Ticket::class)->findOneBy(['id' => $request->query->get('id')]);
        $this->verifGroupUserToTicketGroup($groupTicket);
        $followUp = new FollowUp();
        $followUp->setUsers($this->security->getUser());
        $followUp->setTicket($groupTicket);
        $content = $request->getContent();
        $data = json_decode($content, true);
        $contentValue = $data['content'] ?? null;
        if ($contentValue == "") return new JsonResponse(['hydra:description' => 'The content cannot be blank'], status: 400);
        $followUp->setContent($contentValue);
        $followUp->setCreatedAt(new \DateTimeImmutable());
        $followUp->setUpdateAt(new \DateTimeImmutable());
        $followUp->setDeleted($data['deleted']);
        $followUp->setSolution($data['solution']);
        $this->manager->persist($followUp);
        $this->manager->flush();
        // $data = new JsonResponse('followUp added successfully ', 201);
        $data = $followUp;

        return $data;

    }

    private function getGroupNames($groupTicket, $groupType)
    {
        $method = 'getGroups' . ucfirst($groupType);

        if (!method_exists($groupTicket, $method)) {
            throw new \InvalidArgumentException('Invalid group type provided.');
        }

        $groups = $groupTicket->$method();
        $groups->initialize();

        $groupNames = [];

        foreach ($groups as $group) {
            $groupName = $group->getName();
            $groupNames[] = $groupName;
        }

        return $groupNames;
    }

    private function verifGroupUserToTicketGroup($groupTicket)
    {
        if ($groupTicket == null) {
            throw new BadRequestHttpException("Id Ticket doesn't exist");
        }
        $groupAssigned = $this->getGroupNames($groupTicket, 'assigned');
        $groupObserver = $this->getGroupNames($groupTicket, 'observer');
        $groupApplicant = $this->getGroupNames($groupTicket, 'applicant');
        // dd($this->security->getUser()->getGroups()->getName());

        if (
            !in_array($this->security->getUser()->getGroups()->getName(), $groupAssigned) &&
            !in_array($this->security->getUser()->getGroups()->getName(), $groupObserver) &&
            !in_array($this->security->getUser()->getGroups()->getName(), $groupApplicant)

        ) {
            throw new BadRequestHttpException('User unauthorized');
        }
    }
}
