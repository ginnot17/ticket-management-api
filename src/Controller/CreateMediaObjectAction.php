<?php
// api/src/Controller/CreateMediaObjectAction.php
namespace App\Controller;
use App\Entity\Attachement;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
#[AsController]
final class CreateMediaObjectAction extends AbstractController
{
    public function __invoke(Request $request): Attachement
    {
        $uploadedFile = $request->files->get('file');
        if (!$uploadedFile) {
            throw new BadRequestHttpException('"file" is required');
        }

        // dd($uploadedFile);
        $attachement = new Attachement();
        $attachement->file = $uploadedFile;
        return $attachement;
    }
}