<?php

namespace App\Controller;

use App\Entity\Ticket;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Request;

class TicketController extends AbstractController
{
    public function __construct(
        private EntityManagerInterface $manager,
        private Security               $security
    )
    {
    }

    /**
     *
     * @return array
     */
    public function __invoke(Request $request): array
    {
        // /api/tickets/user OU /api/tickets/group
        $typeFilter = explode("?", explode("/", $request->getRequestUri())[3])[0];
        // dd($typeFilter);
        if ($typeFilter === 'user') {
            $data = $this->manager->getRepository(Ticket::class)->findByUserTicket($this->security->getUser()->getId(), $request->query->get('user_ticket_role'), $request->query->get('type'));
        }else{
            $data = $this->manager->getRepository(Ticket::class)->findByGroupTicket($this->security->getUser()->getGroups()->getId(), $request->query->get('group_ticket_role'), $request->query->get('type'));
        }
        return $data;
    }
}
