<?php

namespace App\EntityListener;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsEntityListener;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

#[AsEntityListener(event: Events::prePersist,entity: User::class)]
#[AsEntityListener(event: Events::preUpdate,entity: User::class)]
class UserEntityListener
{
    private UserPasswordHasherInterface $hash;

    public function __construct(UserPasswordHasherInterface $hash)
    {
        $this->hash=$hash;
    }

    public function prePersist(User $user,LifecycleEventArgs $event):void{
        $this->encodePassword($user);
    }

    public function preUpdate(User $user,LifecycleEventArgs $event):void{
        $this->encodePassword($user);
    }

    public function encodePassword(User $user)
    {
        if ($user->getPassword()===null) return;
        $user->setPassword($this->hash->hashPassword($user,$user->getPassword()));
    }
}