<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Unique;

#[ApiResource(
    normalizationContext: ['groups' => ['read:user']],
    denormalizationContext: ['groups' => ['write:user']],
)]
#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table(name: '`user`')]
#[UniqueEntity("email",message: "This email is already in use.")]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[Groups(['read:ticket','read:followUp'])]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[Groups(['read:ticket','read:followUp','read:user','write:user'])]
    #[ORM\Column(length: 180, unique: true)]
    #[Email]
    #[NotBlank]

    private ?string $email = null;

    #[Groups(['read:ticket','read:followUp','read:user'])]
    #[ORM\Column]
    private array $roles = ["ROLE_USER"];

    /**
     * @var string The hashed password
     */

    #[Groups(['write:user'])]
    #[ORM\Column]
    #[NotBlank]
    private ?string $password = null;

    #[Groups(['read:user'])]
    #[ORM\ManyToMany(targetEntity: Ticket::class, mappedBy: 'observer')]
    #[ORM\JoinTable(name: 'ticket_observer')]
    private Collection $observers;

    #[Groups(['read:user'])]
    #[ORM\ManyToMany(targetEntity: Ticket::class, mappedBy: 'applicant')]
    #[ORM\JoinTable(name: 'ticket_applicant')]
    private Collection $applicants;

    #[Groups(['read:user'])]
    #[ORM\ManyToMany(targetEntity: Ticket::class, mappedBy: 'assigned')]
    #[ORM\JoinTable(name: 'ticket_assigned')]
    private Collection $assigneds;

    #[Groups(['read:user'])]
    #[ORM\OneToMany(mappedBy: 'users', targetEntity: FollowUp::class)]
    private Collection $followUps;

    #[Groups(['read:user','write:user'])]
    #[ORM\ManyToOne(inversedBy: 'users')]
    #[NotBlank]
    private ?Group $groups = null;

    #[Groups(['write:user','read:ticket','read:followUp'])]
    #[ORM\Column(length: 255, nullable: true)]
    #[NotBlank]
    private ?string $firstName = null;


    #[Groups(['write:user','read:ticket','read:followUp'])]
    #[ORM\Column(length: 255, nullable: true)]
    #[NotBlank]
    private ?string $lastName = null;





    public function __construct()
    {
        $this->observers = new ArrayCollection();
        $this->applicants = new ArrayCollection();
        $this->assigneds = new ArrayCollection();
        $this->followUps = new ArrayCollection();


    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): static
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): static
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): static
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials(): void
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return Collection<int, Ticket>
     */
    public function getObservers(): Collection
    {
        return $this->observers;
    }

    public function addObserver(Ticket $observer): static
    {
        if (!$this->observers->contains($observer)) {
            $this->observers->add($observer);
            $observer->addObserver($this);
        }

        return $this;
    }

    public function removeObserver(Ticket $observer): static
    {
        if ($this->observers->removeElement($observer)) {
            $observer->removeObserver($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, Ticket>
     */
    public function getApplicants(): Collection
    {
        return $this->applicants;
    }

    public function addApplicant(Ticket $applicant): static
    {
        if (!$this->applicants->contains($applicant)) {
            $this->applicants->add($applicant);
            $applicant->addApplicant($this);
        }

        return $this;
    }

    public function removeApplicant(Ticket $applicant): static
    {
        if ($this->applicants->removeElement($applicant)) {
            $applicant->removeApplicant($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, Ticket>
     */
    public function getAssigneds(): Collection
    {
        return $this->assigneds;
    }

    public function addAssigned(Ticket $assigned): static
    {
        if (!$this->assigneds->contains($assigned)) {
            $this->assigneds->add($assigned);
            $assigned->addAssigned($this);
        }

        return $this;
    }

    public function removeAssigned(Ticket $assigned): static
    {
        if ($this->assigneds->removeElement($assigned)) {
            $assigned->removeAssigned($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, FollowUp>
     */
    public function getFollowUps(): Collection
    {
        return $this->followUps;
    }

    public function addFollowUp(FollowUp $followUp): static
    {
        if (!$this->followUps->contains($followUp)) {
            $this->followUps->add($followUp);
            $followUp->setUsers($this);
        }

        return $this;
    }

    public function removeFollowUp(FollowUp $followUp): static
    {
        if ($this->followUps->removeElement($followUp)) {
            // set the owning side to null (unless already changed)
            if ($followUp->getUsers() === $this) {
                $followUp->setUsers(null);
            }
        }

        return $this;
    }

    public function getGroups(): ?Group
    {
        return $this->groups;
    }

    public function setGroups(?Group $groups): static
    {
        $this->groups = $groups;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): static
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): static
    {
        $this->lastName = $lastName;

        return $this;
    }

  

    





 
}
