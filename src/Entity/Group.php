<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\GroupRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ApiResource]
#[ORM\Entity(repositoryClass: GroupRepository::class)]
#[ORM\Table(name: '`group`')]
class Group
{
    #[Groups(['read:ticket','read:user'])]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[Groups(['read:ticket','read:user'])]
    #[ORM\Column(length: 255)]
    private ?string $name = null;



    #[ORM\ManyToMany(targetEntity: Ticket::class, inversedBy: 'groupsAssigned')]
    #[ORM\JoinTable(name: 'group_ticket_assigned')]
    private Collection $ticketsAssigned;

    #[ORM\ManyToMany(targetEntity: Ticket::class, inversedBy: 'groupsObserver')]
    #[ORM\JoinTable(name: 'group_ticket_observer')]
    private Collection $ticketsObserver;

    #[ORM\ManyToMany(targetEntity: Ticket::class, inversedBy: 'groupsApplicant')]
    #[ORM\JoinTable(name: 'group_ticket_applicant')]
    private Collection $ticketsApplicant;

    #[ORM\OneToMany(mappedBy: 'groups', targetEntity: User::class)]
    private Collection $users;

    public function __construct()
    {
        $this->ticketsAssigned = new ArrayCollection();
        $this->ticketsObserver = new ArrayCollection();
        $this->ticketsApplicant = new ArrayCollection();
        $this->users = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }



    /**
     * @return Collection<int, Ticket>
     */
    public function getTicketsAssigned(): Collection
    {
        return $this->ticketsAssigned;
    }

    public function addTicketsAssigned(Ticket $ticketsAssigned): static
    {
        if (!$this->ticketsAssigned->contains($ticketsAssigned)) {
            $this->ticketsAssigned->add($ticketsAssigned);
        }

        return $this;
    }

    public function removeTicketsAssigned(Ticket $ticketsAssigned): static
    {
        $this->ticketsAssigned->removeElement($ticketsAssigned);

        return $this;
    }

    /**
     * @return Collection<int, Ticket>
     */
    public function getTicketsObserver(): Collection
    {
        return $this->ticketsObserver;
    }

    public function addTicketsObserver(Ticket $ticketsObserver): static
    {
        if (!$this->ticketsObserver->contains($ticketsObserver)) {
            $this->ticketsObserver->add($ticketsObserver);
        }

        return $this;
    }

    public function removeTicketsObserver(Ticket $ticketsObserver): static
    {
        $this->ticketsObserver->removeElement($ticketsObserver);

        return $this;
    }

    /**
     * @return Collection<int, Ticket>
     */
    public function getTicketsApplicant(): Collection
    {
        return $this->ticketsApplicant;
    }

    public function addTicketsApplicant(Ticket $ticketsApplicant): static
    {
        if (!$this->ticketsApplicant->contains($ticketsApplicant)) {
            $this->ticketsApplicant->add($ticketsApplicant);
        }

        return $this;
    }

    public function removeTicketsApplicant(Ticket $ticketsApplicant): static
    {
        $this->ticketsApplicant->removeElement($ticketsApplicant);

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): static
    {
        if (!$this->users->contains($user)) {
            $this->users->add($user);
            $user->setGroups($this);
        }

        return $this;
    }

    public function removeUser(User $user): static
    {
        if ($this->users->removeElement($user)) {
            // set the owning side to null (unless already changed)
            if ($user->getGroups() === $this) {
                $user->setGroups(null);
            }
        }

        return $this;
    }
}
