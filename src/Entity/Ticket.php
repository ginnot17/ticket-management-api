<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use App\Controller\ApplicantTicketsController;
use App\Controller\AssignedTicketsController;
use App\Controller\ObserverTicketsController;
use App\Controller\TicketController;
use App\Repository\TicketRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use phpDocumentor\Reflection\DocBlock\Tags\Method;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

#[ApiResource(
    operations: [
        new GetCollection(

            uriTemplate: '/tickets/user',
            controller: TicketController::class,
            openapiContext: [
                'parameters' => [
                    [
                        'in' => 'query',
                        'name' => 'user_ticket_role',
                        'description' => 'Role of the user related to the ticket (e.g., assigned, observer, applicant)'
                    ],
                    [
                        'in' => 'query',
                        'name' => 'type',
                        'description' => 'Role of the user related to the ticket (e.g., incident,request)'
                    ],
                ]
            ],
            name: 'ticket_user'
        ),

    ],
    normalizationContext: ['groups' => ['read:ticket']],
    denormalizationContext: ['groups' => ['write:ticket']],
)]
#[ApiResource(
    operations: [
        new GetCollection(
            uriTemplate: '/tickets/group',
            controller: TicketController::class,
            openapiContext: [
                'parameters' => [
                    [
                        'in' => 'query',
                        'name' => 'group_ticket_role',
                        'description' => 'Role of the group related to the ticket (e.g., groupsAssigned, groupsObserver, groupsApplicant)'
                    ],
                ]
            ],
            name: 'ticket_group'
        ),
    ],
    normalizationContext: ['groups' => ['read:ticket']],
    denormalizationContext: ['groups' => ['write:ticket']],
)]
#[ApiResource(
    operations: [
        new Post(),
        new Put(),
        new Get()
    ]
)]
#[ORM\Entity(repositoryClass: TicketRepository::class)]
class Ticket
{
    #[Groups(['read:ticket', 'write:ticket','read:followUp'])]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;


    #[Groups(['read:ticket', 'write:ticket'])]
    #[ORM\Column(length: 255)]
    #[NotBlank]
    #[Length(max: 255)]
    private ?string $title = null;

    #[Groups(['read:ticket', 'write:ticket'])]
    #[ORM\Column(type: Types::TEXT)]
    private ?string $description = null;

    #[Groups(['read:ticket', 'write:ticket'])]
    #[ORM\Column]
    private ?\DateTimeImmutable $CreatedAt = null;

    #[Groups(['read:ticket', 'write:ticket','read:followUp'])]
    #[ORM\ManyToMany(targetEntity: User::class, inversedBy: 'observers')]
    #[ORM\JoinTable(name: 'ticket_observer')]
    private Collection $observer;

    #[Groups(['read:ticket', 'write:ticket','read:followUp'])]
    #[ORM\ManyToMany(targetEntity: User::class, inversedBy: 'applicants')]
    #[ORM\JoinTable(name: 'ticket_applicant')]
    private Collection $applicant;

    #[Groups(['read:ticket', 'write:ticket','read:followUp'])]
    #[ORM\ManyToMany(targetEntity: User::class, inversedBy: 'assigneds')]
    #[ORM\JoinTable(name: 'ticket_assigned')]
    private Collection $assigned;

    #[Groups(['read:ticket', 'write:ticket'])]
    #[ORM\OneToMany(mappedBy: 'ticket', targetEntity: FollowUp::class)]
    private Collection $followUps;

    #[Groups(['read:ticket', 'write:ticket'])]
    #[ORM\ManyToMany(targetEntity: Group::class, mappedBy: 'ticketsAssigned')]
    #[NotBlank]
    private Collection $groupsAssigned;

    #[Groups(['read:ticket', 'write:ticket'])]
    #[ORM\ManyToMany(targetEntity: Group::class, mappedBy: 'ticketsObserver')]
    private Collection $groupsObserver;

    #[Groups(['read:ticket', 'write:ticket'])]
    #[ORM\ManyToMany(targetEntity: Group::class, mappedBy: 'ticketsApplicant')]
    private Collection $groupsApplicant;

    #[ORM\Column(length: 15)]
    #[Groups(['read:ticket', 'write:ticket'])]
    #[NotBlank]
    private ?string $type = null;

    #[ORM\Column(length: 20, nullable: true)]
    #[Groups(['read:ticket', 'write:ticket','read:followUp'])]
    private ?string $status = null;


    public function __construct()
    {
        $this->observer = new ArrayCollection();
        $this->applicant = new ArrayCollection();
        $this->assigned = new ArrayCollection();
        $this->followUps = new ArrayCollection();
        $this->groupsAssigned = new ArrayCollection();
        $this->groupsObserver = new ArrayCollection();
        $this->groupsApplicant = new ArrayCollection();

    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): static
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->CreatedAt;
    }

    public function setCreatedAt(\DateTimeImmutable $CreatedAt): static
    {
        $this->CreatedAt = $CreatedAt;

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getObserver(): Collection
    {
        return $this->observer;
    }

    public function addObserver(User $observer): static
    {
        if (!$this->observer->contains($observer)) {
            $this->observer->add($observer);
        }

        return $this;
    }

    public function removeObserver(User $observer): static
    {
        $this->observer->removeElement($observer);

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getApplicant(): Collection
    {
        return $this->applicant;
    }

    public function addApplicant(User $applicant): static
    {
        if (!$this->applicant->contains($applicant)) {
            $this->applicant->add($applicant);
        }

        return $this;
    }

    public function removeApplicant(User $applicant): static
    {
        $this->applicant->removeElement($applicant);

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getAssigned(): Collection
    {
        return $this->assigned;
    }

    public function addAssigned(User $assigned): static
    {
        if (!$this->assigned->contains($assigned)) {
            $this->assigned->add($assigned);
        }

        return $this;
    }

    public function removeAssigned(User $assigned): static
    {
        $this->assigned->removeElement($assigned);

        return $this;
    }

    /**
     * @return Collection<int, FollowUp>
     */
    public function getFollowUps(): Collection
    {
        return $this->followUps;
    }

    public function addFollowUp(FollowUp $followUp): static
    {
        if (!$this->followUps->contains($followUp)) {
            $this->followUps->add($followUp);
            $followUp->setTicket($this);
        }

        return $this;
    }

    public function removeFollowUp(FollowUp $followUp): static
    {
        if ($this->followUps->removeElement($followUp)) {
            // set the owning side to null (unless already changed)
            if ($followUp->getTicket() === $this) {
                $followUp->setTicket(null);
            }
        }

        return $this;
    }

    public function isAssignedToUser(User $user): bool
    {
        if (($this->getAssigned()->contains($user) || $this->getApplicant()->contains($user)) || $user->getRoles() == ["ROLE_USER", "ROLE_ADMIN"]) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return Collection<int, Group>
     */
    public function getGroupsAssigned(): Collection
    {
        return $this->groupsAssigned;
    }

    public function addGroupsAssigned(Group $groupsAssigned): static
    {
        if (!$this->groupsAssigned->contains($groupsAssigned)) {
            $this->groupsAssigned->add($groupsAssigned);
            $groupsAssigned->addTicketsAssigned($this);
        }

        return $this;
    }

    public function removeGroupsAssigned(Group $groupsAssigned): static
    {
        if ($this->groupsAssigned->removeElement($groupsAssigned)) {
            $groupsAssigned->removeTicketsAssigned($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, Group>
     */
    public function getGroupsObserver(): Collection
    {
        return $this->groupsObserver;
    }

    public function addGroupsObserver(Group $groupsObserver): static
    {
        if (!$this->groupsObserver->contains($groupsObserver)) {
            $this->groupsObserver->add($groupsObserver);
            $groupsObserver->addTicketsObserver($this);
        }

        return $this;
    }

    public function removeGroupsObserver(Group $groupsObserver): static
    {
        if ($this->groupsObserver->removeElement($groupsObserver)) {
            $groupsObserver->removeTicketsObserver($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, Group>
     */
    public function getGroupsApplicant(): Collection
    {
        return $this->groupsApplicant;
    }

    public function addGroupsApplicant(Group $groupsApplicant): static
    {
        if (!$this->groupsApplicant->contains($groupsApplicant)) {
            $this->groupsApplicant->add($groupsApplicant);
            $groupsApplicant->addTicketsApplicant($this);
        }

        return $this;
    }

    public function removeGroupsApplicant(Group $groupsApplicant): static
    {
        if ($this->groupsApplicant->removeElement($groupsApplicant)) {
            $groupsApplicant->removeTicketsApplicant($this);
        }

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): static
    {
        $this->type = $type;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): static
    {
        $this->status = $status;

        return $this;
    }


}
