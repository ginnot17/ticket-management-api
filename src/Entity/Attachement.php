<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Put;
use ApiPlatform\OpenApi\Model;
use App\Controller\CreateMediaObjectAction;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Post;
use App\Repository\AttachementRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Vich\UploaderBundle\Mapping\Annotation as Vich;


#[Vich\Uploadable]
#[ApiResource(
    operations:[
        new Post(
            controller: CreateMediaObjectAction::class,
            deserialize: false,
            openapi: new Model\Operation(
                requestBody: new Model\RequestBody(
                    content: new \ArrayObject([
                        'multipart/form-data' => [
                            'schema' => [
                                'type' => 'object',
                                'properties' => [
                                    'file' => [
                                        'type' => 'string',
                                        'format' => 'binary'
                                    ]
                                ]
                            ]
                        ]
                    ])
                )
            )
        ),
        new Put()
    ],
    normalizationContext: ['groups' => ['read:attachement']],
    denormalizationContext: ['groups' => ['write:attachement']],
)]
#[ORM\Entity(repositoryClass: AttachementRepository::class)]
class Attachement
{
    #[Groups(['read:attachement','read:attachement','read:followUp'])]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ApiProperty(types: ['https://schema.org/contentUrl'])]
    #[Groups(['read:attachement','read:attachement','read:followUp','write:attachement'])]
    #[ORM\Column(type: Types::TEXT, nullable: true)]
    public ?string $url = null;

    #[Vich\UploadableField(mapping: "attachement", fileNameProperty: "filePath")]
    #[Assert\NotNull(groups: ['media_object_create'])]
    public ?File $file = null;

    #[Groups(['read:attachement','read:attachement','read:followUp','write:attachement'])]
    #[ORM\ManyToOne(inversedBy: 'attachements')]
    private ?FollowUp $followUp = null;

    #[Groups(['read:attachement','read:attachement','read:followUp'])]
    #[ORM\Column(length: 255, nullable: true)]
    public ?string $filePath = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): static
    {
        $this->url = $url;

        return $this;
    }

    public function getFollowUp(): ?FollowUp
    {
        return $this->followUp;
    }

    public function setFollowUp(?FollowUp $followUp): static
    {
        $this->followUp = $followUp;

        return $this;
    }

    public function getFilePath(): ?string
    {
        return $this->filePath;
    }

    public function setFilePath(?string $filePath): static
    {
        $this->filePath = $filePath;

        return $this;
    }
}
