<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use App\Controller\FollowUpControllers;
use App\Repository\FollowUpRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\NotBlank;



#[ApiResource(

    operations: [
        new GetCollection(
            uriTemplate: '/followUp/ticket',
            controller: FollowUpControllers::class,
            openapiContext: [
                'parameters' =>
                    [[
                        'in' => 'query',
                        'name' => 'id',
                        'description' => 'id Ticket'
                    ]]
            ],
            name: 'followUp_Ticket_get'

        ),
        new Post(
            uriTemplate: '/followUp/ticket',
            controller: FollowUpControllers::class,
            openapiContext: [
                'parameters' =>
                    [[
                        'in' => 'query',
                        'name' => 'id',
                        'description' => 'id Ticket'
                    ]]
            ],
            name: 'followUp_Ticket_post',
        ),
        new Put()

    ],

    normalizationContext: ['groups' => ['read:followUp']],
    denormalizationContext: ['groups' => ['write:followUp']],

)]
#[ORM\Entity(repositoryClass: FollowUpRepository::class)]
class FollowUp
{
    #[Groups(['read:followUp', 'write:followUp', 'read:ticket'])]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;


    #[Groups(['read:followUp', 'write:followUp', 'read:ticket'])]
    #[ORM\Column(type: Types::TEXT)]
    #[NotBlank]
    private ?string $content = null;

    #[Groups(['read:followUp', 'write:followUp', 'read:ticket'])]
    #[ORM\Column]
    private ?\DateTimeImmutable $CreatedAt = null;

    #[Groups(['read:followUp', 'write:followUp', 'read:ticket'])]
    #[ORM\Column]
    private ?\DateTimeImmutable $UpdateAt = null;

    #[Groups(['read:followUp', 'write:followUp', 'read:ticket'])]
    #[ORM\ManyToOne(inversedBy: 'followUps')]
    private ?User $users = null;

    #[Groups(['read:followUp', 'write:followUp'])]
    #[ORM\ManyToOne(inversedBy: 'followUps')]
    private ?Ticket $ticket = null;

    #[Groups(['read:followUp', 'write:followUp', 'read:ticket'])]
    #[ORM\OneToMany(mappedBy: 'followUp', targetEntity: Attachement::class)]
    private Collection $attachements;


    #[Groups(['read:followUp', 'write:followUp', 'read:ticket'])]
    #[ORM\Column(nullable: true)]
    private ?int $deleted = null;

    #[ORM\Column(nullable: true)]
    #[Groups(['read:followUp', 'write:followUp', 'read:ticket'])]
    private ?int $solution = null;

    public function __construct()
    {
        $this->attachements = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): static
    {
        $this->content = $content;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->CreatedAt;
    }

    public function setCreatedAt(\DateTimeImmutable $CreatedAt): static
    {
        $this->CreatedAt = $CreatedAt;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeImmutable
    {
        return $this->UpdateAt;
    }

    public function setUpdateAt(\DateTimeImmutable $UpdateAt): static
    {
        $this->UpdateAt = $UpdateAt;

        return $this;
    }

    public function getUsers(): ?User
    {
        return $this->users;
    }

    public function setUsers(?User $users): static
    {
        $this->users = $users;

        return $this;
    }

    public function getTicket(): ?Ticket
    {
        return $this->ticket;
    }

    public function setTicket(?Ticket $ticket): static
    {
        $this->ticket = $ticket;

        return $this;
    }

    /**
     * @return Collection<int, Attachement>
     */
    public function getAttachements(): Collection
    {
        return $this->attachements;
    }

    public function addAttachement(Attachement $attachement): static
    {
        if (!$this->attachements->contains($attachement)) {
            $this->attachements->add($attachement);
            $attachement->setFollowUp($this);
        }

        return $this;
    }

    public function removeAttachement(Attachement $attachement): static
    {
        if ($this->attachements->removeElement($attachement)) {
            // set the owning side to null (unless already changed)
            if ($attachement->getFollowUp() === $this) {
                $attachement->setFollowUp(null);
            }
        }

        return $this;
    }

    public function getDeleted(): ?int
    {
        return $this->deleted;
    }

    public function setDeleted(?int $deleted): static
    {
        $this->deleted = $deleted;

        return $this;
    }

    public function getSolution(): ?int
    {
        return $this->solution;
    }

    public function setSolution(?int $solution): static
    {
        $this->solution = $solution;

        return $this;
    }
}
