<?php

namespace App\EventSubscriber;

use App\Entity\User;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Uid\Uuid;

class JWTSubscriber implements EventSubscriberInterface
{
    private $managerRegistry;

    public function __construct(EntityManagerInterface $managerRegistry)
    {
        $this->managerRegistry = $managerRegistry;
    }

    public function onLexikJwtAuthenticationOnJwtCreated(JWTCreatedEvent $event): void
    {
        $data = $event->getData();
        $user = $event->getUser();
        if ($user instanceof User) {
            $data['lastname'] = ($user->getLastName());
            $data['firstname'] = ($user->getFirstName());
            $data['id'] = ($user->getId());
            $data['idGroup'] = ($user->getGroups()->getId());
        }

        $event->setData($data);
    }

    public static function getSubscribedEvents(): array
    {
        return [
            'lexik_jwt_authentication.on_jwt_created' => 'onLexikJwtAuthenticationOnJwtCreated',
        ];
    }
}
