<?php

namespace App\Repository;

use App\Entity\Ticket;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Ticket>
 *
 * @method Ticket|null find($id, $lockMode = null, $lockVersion = null)
 * @method Ticket|null findOneBy(array $criteria, array $orderBy = null)
 * @method Ticket[]    findAll()
 * @method Ticket[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TicketRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Ticket::class);
    }

    /**
     * @return array
     */
    public function findByUserTicket($IdUser, $userTicketRole, $type): array
    {
        (is_null($userTicketRole)) && $userTicketRole = 'assigned';
        (is_null($type)) && $type = 'incident';
        // dd($userTicketRole);
        return $this->createQueryBuilder('t')
            ->join("t.$userTicketRole", "$userTicketRole")
            ->andWhere("$userTicketRole.id= :val AND t.type= '$type'")
            ->setParameter('val', $IdUser)
            ->orderBy('t.id', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function findByGroupTicket($IdGroup, $GroupTicketRole, $type): array
    {
        (is_null($GroupTicketRole)) && $GroupTicketRole = 'groupsAssigned';
        // dd($userTicketRole);
        (is_null($type)) && $type = 'incident';
        return $this->createQueryBuilder('t')
            ->join("t.$GroupTicketRole", "$GroupTicketRole")
            ->andWhere("$GroupTicketRole.id= :val AND t.type= '$type'")
            ->setParameter('val', $IdGroup)
            ->orderBy('t.id', 'ASC')
            ->getQuery()
            ->getResult();
    }

}
