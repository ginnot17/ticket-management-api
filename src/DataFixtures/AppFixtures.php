<?php

namespace App\DataFixtures;

use App\Entity\FollowUp;
use App\Entity\Group;
use App\Entity\Ticket;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');

//        $groups = [];
//        foreach (['IT', 'DEV', 'SYS'] as $key => $value) {
//            $group = new Group();
//            $group->setName($value);
//            $manager->persist($group);
//            $groups[] = $group;
//        }
//
//
//        $users = [];
//        for ($i = 0; $i < 5; $i++) {
//            $user = new User();
//            $email = $faker->email;
//            $user->setEmail($email)
//                ->setRoles(['ROLE_USER'])
//                ->setPassword($this->passwordHasher->hashPassword($user, $email))
//                ->setGroups($manager->getRepository(Group::class)->find($faker->randomElement([7, 8, 9])));
//            $manager->persist($user);
//            $users[] = $user;
//        }


        $users = $manager->getRepository(User::class)->findAll();

        $tickets = [];
        for ($i = 0; $i < 10; $i++) {
            $ticket = new Ticket();
            $ticket->setTitle($faker->sentence)
                ->setDescription($faker->text)
                ->setCreatedAt(\DateTimeImmutable::createFromMutable($faker->dateTime));


            // Ajouter des groupes observateurs
            for ($o = 0; $o < 3; $o++) {
                $randomUser = $faker->randomElement($users);
                $group_observer = $faker->randomElement([$randomUser->getGroups()]);
                $group_random = $manager->getRepository(Group::class)->find($group_observer);
                $ticket->addGroupsObserver($group_random);
            }

            // Ajouter des groupes demandeurs
            for ($o = 0; $o < 3; $o++) {
                $randomUser = $faker->randomElement($users);
                $group_applicant = $faker->randomElement([$randomUser->getGroups()]);
                $group_random = $manager->getRepository(Group::class)->find($group_applicant);
                $ticket->addGroupsApplicant($group_random);
            }

            // Ajouter des groupes attribués
            for ($o = 0; $o < 3; $o++) {
                $randomUser = $faker->randomElement($users);
                $group_assigned = $faker->randomElement([$randomUser->getGroups()]);
                $group_random = $manager->getRepository(Group::class)->find($group_assigned);
                $ticket->addGroupsAssigned($group_random);
            }

            $manager->persist($ticket);
            $tickets[] = $ticket;
        }
        $manager->flush();


    }
}
